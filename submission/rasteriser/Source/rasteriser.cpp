#include <iostream>
#include <algorithm>
#include <glm/glm.hpp>
#include <SDL.h>
#include <stdint.h>
#include "SDLauxiliary.h"
#include "TestModelH.h"

using namespace std;
using glm::vec2;
using glm::vec3;
using glm::mat3;
using glm::vec4;
using glm::mat4;
using glm::ivec2;
using glm::mat4x4;

SDL_Event event;

#define SCREEN_WIDTH 500
#define SCREEN_HEIGHT 500
#define FULLSCREEN_MODE false

struct Pixel
{
  int x;
  int y;
  float zinv;
  vec4 pos3d;
};

struct Vertex
{
  vec4 position;
};

enum Planes {Top = 8, Bottom = 4, Right = 2, Left = 1};

enum Corners {TopLeft = 8, TopRight = 4, BottomLeft = 2, BottomRight = 1};
/* ----------------------------------------------------------------------------*/
/* Global variables                                                            */

vector<Triangle> triangles;
float focalLength = SCREEN_HEIGHT/2.0f;
vec4 cameraPos( 0, 0, -3.001, 1.0);
mat4 R = {{1,0,0,0},
           {0,1,0,0},
           {0,0,1,0},
           {0,0,0,1}};
float yaw = 0;
float moveDistance = 0.2;
float rotationStep = 0.2;
float depthBuffer[SCREEN_HEIGHT+1][SCREEN_WIDTH+1];

bool print = true;
bool drawDepth = true;

vec4 fixedLightPos(0,-0.5,-0.7, 1);
vec4 lightPos;
vec3 lightPower = 20.1f*vec3( 1, 1, 1 );
vec3 indirectLightPowerPerArea = 0.5f*vec3( 1, 1, 1 );

vec4 currentNormal;
vec3 currentReflectance;

/* ----------------------------------------------------------------------------*/
/* FUNCTIONS                                                                   */

bool Update();
void Draw(screen* screen);
void VertexShader( const Vertex& v, Pixel& p );
void PixelShader( screen* screen, const Pixel& p );
void TransformationMatrix( mat4x4& t );
void PrintMatrix( mat4x4 t );
void PrintVector( vec4 v );
void Interpolate( ivec2 a, ivec2 b, vector<ivec2>& result );
void Interpolate( Pixel a, Pixel b, vector<Pixel>& result );
float InterpolateZinv( Pixel a, Pixel b, Pixel m );
vec4 Interpolate3D( Pixel a, Pixel b, Pixel m );
void DrawLineSDL( screen* screen, ivec2 a, ivec2 b, vec3 color );
void DrawPolygonEdges( screen* screen, const vector<vec4>& vertices );
void ComputePolygonRows( const vector<Pixel>& vertexPixels,
                         vector<Pixel>& leftPixels,
                         vector<Pixel>& rightPixels );
void DrawPolygonRows( screen* screen, const vector<Pixel>& leftPixels,
              const vector<Pixel>& rightPixels );
void DrawTriPolygon( screen* screen, const vector<Pixel>& vertices );
void DrawDepth( screen* screen );

int GenerateOutcode(Pixel p);

bool ClipVertices( vector<Pixel> &vertices );
void DrawNSidedPolygon( screen* screen, const vector<Pixel>& vertices );

float CalculateArea(Pixel v1, Pixel v2, Pixel v3, Pixel corner);

/*-----------------------------------------------------------------------------*/

/* Options----------------------------------------------------------------------*/
bool rotateCamera = true;

/*-----------------------------------------------------------------------------*/
int main( int argc, char* argv[] )
{
  screen *depthScreen = InitializeSDL( SCREEN_WIDTH, SCREEN_HEIGHT, FULLSCREEN_MODE );
  if(!drawDepth) KillSDL(depthScreen);
  screen *outputScreen = InitializeSDL( SCREEN_WIDTH, SCREEN_HEIGHT, FULLSCREEN_MODE );

  LoadTestModel( triangles );

  while ( Update() )
    {
      if(drawDepth) DrawDepth(depthScreen);
      Draw(outputScreen);
      SDL_Renderframe(outputScreen);
      if(drawDepth) SDL_Renderframe(depthScreen);
    }

  SDL_SaveImage( outputScreen, "screenshot.bmp" );
  if(drawDepth) SDL_SaveImage(depthScreen, "depthBuffer.bmp");

  KillSDL(outputScreen);
  if(drawDepth) KillSDL(depthScreen);
  return 0;
}

/*Place your drawing here*/
void Draw(screen* screen)
{

  if(rotateCamera) lightPos = (fixedLightPos - cameraPos) * R;
  else lightPos = (fixedLightPos - cameraPos);
  /* Clear buffer */
  memset(screen->buffer, 0, screen->height*screen->width*sizeof(uint32_t));
  for( int y=0; y < SCREEN_HEIGHT; y++){
    for( int x=0; x<SCREEN_WIDTH; x++){
      depthBuffer[y][x] = -numeric_limits<int>::max();
    }
  }

  //Get each polygons vertex
  for( uint32_t i=0; i<triangles.size(); i++)
  {
    currentNormal = triangles[i].normal;
    currentReflectance = triangles[i].color;

    vector<Vertex> vertices(3);

    vertices[0].position    = triangles[i].v0;
    vertices[1].position    = triangles[i].v1;
    vertices[2].position    = triangles[i].v2;
    //shade the vertices
    int V = vertices.size();
    vector<Pixel> vertexPixels( V );
    for( int i = 0; i<V; i++){
      VertexShader( vertices[i], vertexPixels[i] );
    }

    //Clip the edges giving back n-sided polygons
    bool onScreen = ClipVertices( vertexPixels );
    if(onScreen == true){
      //Call DrawPolygon which splits the polygons up into tries and draws them
      if(vertexPixels.size() == 3){
        DrawTriPolygon( screen, vertexPixels );
      }
      else{
        DrawNSidedPolygon( screen, vertexPixels );
      }
    }
  }
}

bool isCorner(Pixel maybeCorner){
  if(maybeCorner.x == 0 || maybeCorner.x == SCREEN_WIDTH){
    if(maybeCorner.y == 0 || maybeCorner.y == SCREEN_HEIGHT){
      return true;
    }
  }
  return false;
}

bool isOnEdge(Pixel maybeEdge){
  if(maybeEdge.x == 0 || maybeEdge.x == SCREEN_WIDTH || maybeEdge.y == 0 || maybeEdge.y == SCREEN_HEIGHT){
    return true;
  }
  return false;
}

vector<Pixel> ConvexHull(vector<Pixel> vertexPixels){
  vector<Pixel> convexHull;
  int leftMost = 0;
  for(uint i=0; i<vertexPixels.size(); i++){
    leftMost = vertexPixels[i].x < vertexPixels[leftMost].x ? i : leftMost;
  }
  Pixel pointOnHull = vertexPixels[leftMost];
  int i = 0;
  Pixel endpoint;
  while(true){
    convexHull.push_back(pointOnHull);
    endpoint = vertexPixels[0];
    for(uint j=1; j<vertexPixels.size(); j++){
      float position = (float) ((endpoint.x - convexHull[i].x)*(vertexPixels[j].y - convexHull[i].y) -
                    (endpoint.y - convexHull[i].y)*(vertexPixels[j].x - convexHull[i].x));
      if((endpoint.x == pointOnHull.x && endpoint.y == pointOnHull.y) || position > 0){
        endpoint = vertexPixels[j];
      }
    }
    i++;
    pointOnHull = endpoint;
    if(endpoint.x == convexHull[0].x && endpoint.y == convexHull[0].y) break;
  }

  return convexHull;

}

void DrawNSidedPolygon( screen* screen, const vector<Pixel>& vertexPixels ){
  if(vertexPixels.size() == 4){
    // cout << "4 sided shape" << endl;
    int corner = 3;
    if(isCorner(vertexPixels[corner])){
      int nonEdge1;
      bool foundNonEdge1 = false;
      bool twoEdges = false;
      int edge1;
      bool foundEdge1 = false;
      int edge2;
      for(int i=0; i<corner; i++){
        if(!isOnEdge(vertexPixels[i])){
          if(!foundNonEdge1){
            foundNonEdge1 = true;
            nonEdge1 = i;
          }
        }
        else{
          if(!foundEdge1){
            foundEdge1 = true;
            edge1 = i;
          }
          else{
            twoEdges = true;
            edge2 = i;
          }
        }
      }

      vector<Pixel> poly1(3);
      vector<Pixel> poly2(3);

      if(twoEdges){
        poly1[0] = vertexPixels[edge1];
        poly1[1] = vertexPixels[nonEdge1];
        poly1[2] = vertexPixels[corner];

        poly2[0] = vertexPixels[edge2];
        poly2[1] = vertexPixels[nonEdge1];
        poly2[2] = vertexPixels[corner];
      }
      else{
        // Find point in screen that is diagonally opposite corner
        vector<Pixel> hull = ConvexHull(vertexPixels);
        int corner;
        for(uint i=0; i<hull.size(); i++){
          if(isCorner(hull[i])) corner = i;
        }

        poly1[0] = hull[(corner+(hull.size()-1))%hull.size()];
        poly1[1] = hull[corner];
        poly1[2] = hull[(corner+2)%hull.size()];

        poly2[0] = hull[(corner+1)%hull.size()];
        poly2[1] = hull[corner];
        poly2[2] = hull[(corner+2)%hull.size()];
      }

      DrawTriPolygon(screen, poly1);
      DrawTriPolygon(screen, poly2);
    }
    else
    {
      vector<Pixel> poly1(3);
      poly1[0] = vertexPixels[0];
      poly1[1] = vertexPixels[1];
      poly1[2] = vertexPixels[2];

      vector<Pixel> poly2(3);
      poly2[0] = vertexPixels[0];
      poly2[1] = vertexPixels[2];
      poly2[2] = vertexPixels[3];

      DrawTriPolygon( screen, poly1 );
      DrawTriPolygon( screen, poly2 );
    }
  }
  else if(vertexPixels.size() == 5){ //TODO test this
    // cout << "5 sided shape" << endl;
    int corner = 4;
    if(isCorner(vertexPixels[corner])){ //if the fourth one is a corner
      bool twoCorners = false;
      for(int i=0; i<4; i++){
        if(isCorner(vertexPixels[i])){
          twoCorners = true;
        }
      }
      if(twoCorners){
        // cout << "Two corners" << endl;

        vector<Pixel> hull = ConvexHull(vertexPixels);

        int nonEdge;
        for(int i = 0; i < 4; i++){
          if(!isOnEdge(hull[i])) nonEdge = i;
        }

        int x = vertexPixels[corner].x;
        int y = hull[nonEdge].y;
        float zinv = vertexPixels[corner].zinv;
        vec4 pos3d = vertexPixels[corner].pos3d;
        Pixel midpoint = {x, y, zinv, pos3d};

        vector<Pixel> poly1(4);
        vector<Pixel> poly2(4);

        poly1[0] = hull[nonEdge];
        poly1[1] = hull[(nonEdge+1)%hull.size()];
        poly1[2] = midpoint;
        poly1[3] = hull[(nonEdge+2)%hull.size()]; // corner

        poly2[0] = hull[nonEdge];
        poly2[1] = hull[(nonEdge+(hull.size()-1))%hull.size()];
        poly2[2] = midpoint;
        poly2[3] = hull[(nonEdge+(hull.size()-2))%hull.size()]; // corner

        DrawNSidedPolygon(screen, poly1);
        DrawNSidedPolygon(screen, poly2);
      }
      else{
        //Identify 2 edge pixels
        int nonEdge1;
        bool foundNonEdge1 = false;
        int nonEdge2;
        for(int i = 0; i < 4; i++){ //Doesn't go through corner
          if(!isOnEdge(vertexPixels[i])){
            if(!foundNonEdge1){
              foundNonEdge1 = true;
              nonEdge1 = i;
            }
            else nonEdge2 = i;
          }
        }
        int x = vertexPixels[nonEdge1].x + 0.5f*(vertexPixels[nonEdge2].x-vertexPixels[nonEdge1].x);
        int y = vertexPixels[nonEdge1].y + 0.5f*(vertexPixels[nonEdge2].y-vertexPixels[nonEdge1].y);
        Pixel midpoint = {x,y,0,vec4(0,0,0,0)};
        midpoint.zinv = InterpolateZinv(vertexPixels[nonEdge1], vertexPixels[nonEdge2], midpoint);
        midpoint.pos3d = Interpolate3D(vertexPixels[nonEdge1], vertexPixels[nonEdge2], midpoint);

        vector<Pixel> hull = ConvexHull(vertexPixels);
        int corner;
        for(uint i=0; i<hull.size(); i++){
          if(isCorner(hull[i])) corner = i;
        }

        vector<Pixel> poly1(4);
        vector<Pixel> poly2(4);

        poly1[0] = hull[(corner+1)%hull.size()];
        poly1[1] = hull[(corner+2)%hull.size()];
        poly1[2] = midpoint;
        poly1[3] = hull[corner];

        poly2[0] = hull[(corner+(hull.size()-1))%hull.size()];
        poly2[1] = hull[(corner+(hull.size()-2))%hull.size()];
        poly2[2] = midpoint;
        poly2[3] = hull[corner];

        DrawNSidedPolygon(screen, poly1);
        DrawNSidedPolygon(screen, poly2);
      }
    }
    else{
      // cout << "Else" << endl;

      vector<Pixel> poly1(3);
      poly1[0] = vertexPixels[0];
      poly1[1] = vertexPixels[1];
      poly1[2] = vertexPixels[2];

      vector<Pixel> poly2(3);
      poly2[0] = vertexPixels[0];
      poly2[1] = vertexPixels[2];
      poly2[2] = vertexPixels[3];

      vector<Pixel> poly3(3);
      poly3[0] = vertexPixels[0];
      poly3[1] = vertexPixels[3];
      poly3[2] = vertexPixels[4];

      DrawTriPolygon( screen, poly1 );
      DrawTriPolygon( screen, poly2 );
      DrawTriPolygon( screen, poly3 );
    }
  }
  else if(vertexPixels.size() == 6){
    // cout << "6 Sided Big Kahuna" <<endl;
  }
  else if (vertexPixels.size() == 7){
    // cout << "7 Sided polyzilla" << endl;
  }
  else{
    cout << "More than 7 sided shape error" << endl;
  }
}

//given 3 shaded vertices, draw the polygon
void DrawTriPolygon( screen* screen, const vector<Pixel>& vertexPixels ){
  // cout << "3 sided shape" << endl;
  vector<Pixel> leftPixels;
  vector<Pixel> rightPixels;
  ComputePolygonRows( vertexPixels, leftPixels, rightPixels );
  DrawPolygonRows( screen, leftPixels, rightPixels );
}

//Returns the projection matrix
void ProjectionMatrix( mat4x4& t ){
  t = { 1, 0, 0,             0,
        0, 1, 0,             0,
        0, 0, 1,             0,
        0, 0, 1/focalLength, 0};
}

//Number returns whether some clipping needs to be done
void VertexShader( const Vertex& v, Pixel& p )
{
  mat4x4 t;
  TransformationMatrix(t);

  //Transform from World space to camera space
  // is this homogeneous
  vec4 v_prime = v.position * t;
  vec4 homoGeneous = vec4(v_prime.x, v_prime.y, v_prime.z, v_prime.z/focalLength);

  //TODO: Could do Frustrum Clipping Here
  //Clip x and y plane of view frustrum
  //  -w . x_max  <= x <= w . x_max
  //  -w . y_max  <= y <= w. y_max

  //Carls reccomended tutorial: http://www.lighthouse3d.com/tutorials/view-frustum-culling/clip-space-approach-extracting-the-planes/

  //Project from clipping space to regular space
  mat4x4 m;
  ProjectionMatrix(m);
  vec4 perspective = homoGeneous * m;
  v_prime = perspective;

  // NOTE: We know this limits our ability to clip due to screen space clipping
  if(v_prime.z < 0) cout << "Behind camera" << endl;
  float x = focalLength * (v_prime.x / v_prime.z) + SCREEN_WIDTH/2.0f;
  float y = focalLength * (v_prime.y / v_prime.z) + SCREEN_HEIGHT/2.0f;

  p.x = x;
  p.y = y;
  p.zinv = 1/(float)v_prime.z;

  p.pos3d = v_prime;
}

void PixelShader( screen* screen, const Pixel& p )
{
  vec4 r = vec4(lightPos.x-p.pos3d.x, lightPos.y-p.pos3d.y,
                lightPos.z-p.pos3d.z, 0);
  r = glm::normalize(r); //should be normalized
  float radius = sqrt(pow((lightPos.x - p.pos3d.x), 2) + //distance from light
                pow((lightPos.y - p.pos3d.y), 2) +       //to intersection
                pow((lightPos.z - p.pos3d.z), 2));       //point

  vec3 D = lightPower * max(dot(r,currentNormal),0.0f)/(float)(4*M_PI*pow(radius,2));

  PutPixelSDL( screen, p.x, p.y, currentReflectance * (D + indirectLightPowerPerArea));
}

void TransformationMatrix( mat4x4& t )
{
  // mat4x4 c = {1,0,0,cameraPos.x,
  //             0,1,0,cameraPos.y,
  //             0,0,1,cameraPos.z,
  //             0,0,0,1};
  mat4x4 c_minus = {1,0,0,-cameraPos.x,
                    0,1,0,-cameraPos.y,
                    0,0,1,-cameraPos.z,
                    0,0,0,1};
  //t = c * R * c_minus;
  if(rotateCamera) t = c_minus * R;
  else t = R * c_minus;
}

void PrintMatrix( mat4x4 t )
{
  for(int i=0; i<4; i++){
    for(int j=0; j<4; j++){
      std::cout << t[i][j] << ", ";
    }
    std::cout << std::endl;
  }
}

void PrintVector( vec4 v )
{
    for(int j=0; j<4; j++){
      std::cout << v[j] << ", ";
    }
    std::cout << std::endl;
}

void Interpolate( ivec2 a, ivec2 b, vector<ivec2>& result)
{
  int N = result.size();
  vec2 step = vec2(b-a) / float(max(N-1,1));
  vec2 current( a );
  for( int i=0; i<N; i++){
    result[i] = current;
    current += step;
  }
}

//interpolates all values between two pixels. Size is based on result
void Interpolate( Pixel a, Pixel b, vector<Pixel>& result)
{
  int N = result.size();
  float xStep = (b.x - a.x) / float(max(N-1,1));
  float yStep = (b.y - a.y) / float(max(N-1,1));
  vec4 posStep = ((b.pos3d*b.zinv) - (a.pos3d*a.zinv)) /float(max(N-1,1));
  float q;
  float currentX = a.x;
  float currentY = a.y;
  vec4 currentPos = a.pos3d * a.zinv;
  for( int i=0; i<N; i++){
    q = i / (float) max(N-1,1);
    result[i].x = currentX;
    result[i].y = currentY;
    result[i].zinv = (float) a.zinv + (q * (b.zinv - a.zinv));
    result[i].pos3d = currentPos/result[i].zinv;
    currentX += xStep;
    currentY += yStep;
    currentPos += posStep;
  }
}

//Draws a line between two points
void DrawLineSDL( screen* screen, ivec2 a, ivec2 b, vec3 color )
{
  ivec2 delta = glm::abs( a - b );
  int pixels = glm::max( delta.x, delta.y ) + 1;

  vector<ivec2> line( pixels );
  Interpolate( a, b, line );
  for(uint i=0; i<line.size(); i++){
    PutPixelSDL( screen, line[i].x, line[i].y, color);
  }
}

//Draws a polygons edges
void DrawTriPolygonEdges( screen* screen, const vector<Vertex>& vertices )
{
  int V = vertices.size();

  // Transform each vertex from 3D world position to 2D image position
  vector<Pixel> projectedVertices( V );
  for( int i =0; i<V; i++)
  {
    VertexShader( vertices[i], projectedVertices[i] );
  }

  //Loop over all vertices and draw the edge from it to the next vertex:
  for( int i=0; i<V; i++){
    int j = (i+1)%V; // The next vertex
    vec3 color(1,1,1);
    ivec2 a = ivec2(projectedVertices[i].x,projectedVertices[i].y);
    ivec2 b = ivec2(projectedVertices[j].x,projectedVertices[j].y);
    DrawLineSDL( screen, a, b, color);
  }
}

void ComputePolygonRows( const vector<Pixel>& vertexPixels,
                         vector<Pixel>& leftPixels,
                         vector<Pixel>& rightPixels )
{
  // 1. Find max and min y-value of the polygon
  //    and compute the number of rows it occupies.
  int maxY = -numeric_limits<int>::max();
  int minY = +numeric_limits<int>::max();
  for(uint i=0; i<vertexPixels.size(); i++){
    if( vertexPixels[i].y < minY ) minY = vertexPixels[i].y;
    if( vertexPixels[i].y > maxY ) maxY = vertexPixels[i].y;
  }

  int rows = maxY - minY;

  // 2. Resize leftPixels and RightPixels
  //    so that they have and element for each row
  leftPixels.resize(rows+1);
  rightPixels.resize(rows+1);

  // 3. Initialize the x-coordinates in leftPixels
  //    to some really large value and the x-coordinates
  //    in rightPixles to some really small value
  for( int i=0; i<rows+1; i++ ){
    leftPixels[i].x = +numeric_limits<int>::max();
    rightPixels[i].x = -numeric_limits<int>::max();
  }

  // 4. loop through all edges of the polygon and use
  //    linear interpolation to find the x-coordinate for
  //    each row it occupies. Update the corresponding
  //    values in rightPixles and leftPixels

  vector<Pixel> edge1(abs(vertexPixels[0].y - vertexPixels[1].y)+1);
  vector<Pixel> edge2(abs(vertexPixels[1].y - vertexPixels[2].y)+1);
  vector<Pixel> edge3(abs(vertexPixels[0].y - vertexPixels[2].y)+1);

  Interpolate(vertexPixels[0],vertexPixels[1], edge1);
  Interpolate(vertexPixels[1],vertexPixels[2], edge2);
  Interpolate(vertexPixels[0],vertexPixels[2], edge3);

  for( int y=minY; y<=maxY; y++){
    leftPixels[y-minY].y = y;
    rightPixels[y-minY].y = y;
    for(uint i=0; i<edge1.size(); i++){
      if( edge1[i].y == y ){
        if( edge1[i].x < leftPixels[y-minY].x ) {
          leftPixels[y-minY].x = edge1[i].x;
          leftPixels[y-minY].zinv = edge1[i].zinv;
          leftPixels[y-minY].pos3d = edge1[i].pos3d;
        }
        if( edge1[i].x > rightPixels[y-minY].x ){
          rightPixels[y-minY].x = edge1[i].x;
          rightPixels[y-minY].zinv = edge1[i].zinv;
          rightPixels[y-minY].pos3d = edge1[i].pos3d;
        }
      }
    }
    for(uint i=0; i<edge2.size(); i++){
      if( edge2[i].y == y ){
        if( edge2[i].x < leftPixels[y-minY].x ) {
          leftPixels[y-minY].x = edge2[i].x;
          leftPixels[y-minY].zinv = edge2[i].zinv;
          leftPixels[y-minY].pos3d = edge2[i].pos3d;
        }
        if( edge2[i].x > rightPixels[y-minY].x ) {
          rightPixels[y-minY].x = edge2[i].x;
          rightPixels[y-minY].zinv = edge2[i].zinv;
          rightPixels[y-minY].pos3d = edge2[i].pos3d;
        }
      }
    }
    for(uint i=0; i<edge3.size(); i++){
      if( edge3[i].y == y ){
        if( edge3[i].x < leftPixels[y-minY].x ) {
          leftPixels[y-minY].x = edge3[i].x;
          leftPixels[y-minY].zinv = edge3[i].zinv;
          leftPixels[y-minY].pos3d = edge3[i].pos3d;
        }
        if( edge3[i].x > rightPixels[y-minY].x ) {
          rightPixels[y-minY].x = edge3[i].x;
          rightPixels[y-minY].zinv = edge3[i].zinv;
          rightPixels[y-minY].pos3d = edge3[i].pos3d;
        }
      }
    }
  }
}

//Using the left and right pixel rows of the polygon, fill the polygon
void DrawPolygonRows( screen* screen, const vector<Pixel>& leftPixels,
               const vector<Pixel>& rightPixels )
{
  for(uint i=0; i < leftPixels.size(); i++){
    vector<Pixel> pixelRow(rightPixels[i].x - leftPixels[i].x + 1);
    Interpolate(leftPixels[i], rightPixels[i], pixelRow);
    for(uint j=0; j < pixelRow.size(); j++){
      if((pixelRow[j].y < 0) || (pixelRow[j].y > SCREEN_HEIGHT) || (pixelRow[j].x < 0) || (pixelRow[j].x > SCREEN_WIDTH)){
        cout << "OUT OF BOUNDS" << endl;
      }
      if(depthBuffer[pixelRow[j].y][pixelRow[j].x] < pixelRow[j].zinv){
        PixelShader(screen, pixelRow[j]);
        depthBuffer[pixelRow[j].y][pixelRow[j].x] = pixelRow[j].zinv;
      }
    }
  }
}

//Takes in a pixel and generates the outcode for use in clipping
int GenerateOutcode(Pixel p){

  int isClip = 0;
  if(p.x<0) isClip += 1;
  else if (p.x > SCREEN_WIDTH) isClip += 2;
  if(p.y<0) isClip += 4;
  else if(p.y > SCREEN_HEIGHT) isClip += 8;

  return isClip;
}

//Given two endpoints of a line and the coordinates of the desired zinv value
//returns the zinv value FIXME
float InterpolateZinv( Pixel a, Pixel b, Pixel m ){
  //Are these the needed things
  float q = sqrt(pow((a.x - m.x),2) + pow((a.y - m.y),2)) /
    (float) sqrt(pow((a.x - b.x),2) + pow((a.y - b.y),2));
  return a.zinv + (q * (b.zinv - a.zinv));
}

vec4 Interpolate3D( Pixel a, Pixel b, Pixel m){
  float q = sqrt(pow((a.x - m.x),2) + pow((a.y - m.y),2)) /
    (float) sqrt(pow((a.x - b.x),2) + pow((a.y - b.y),2));
  vec4 posStep = ((b.pos3d*b.zinv) - (a.pos3d*a.zinv));
  return ((a.pos3d*a.zinv) + (q * posStep))/m.zinv;
}

//Takes in two vertices, their opcodes and a list of clipped vertices to add to.
//if the first point needs clipping.
//  if it does not: it adds it to the clipped list, it
//  if it does: clip it and add it to the clipped list
//then checks if the second point needs clipping
//  if it does it clips it and adds it to the list
//  if it does not it ignores it (this is to avoid duplicate vertices)
void ClipLine( Pixel v1, int out1, Pixel v2, int out2, vector<Pixel> &vertices){
  if((out1 | out2 ) != 0){ //Needs clipping
    if((out1 & out2 ) == 0){ //Checks it is on screen at all

      float m = (v2.y-v1.y)/(float)(v2.x-v1.x); //calculate gradient

      if((out1 | 0) == 0){ //if the first pixel doesn't need clipping, add it
        vertices.resize(vertices.size() + 1);
        vertices[vertices.size() - 1] = v1;
      }
      else{                //otherwise clip first pixel and add it
        Pixel newV1;
        if((out1 & Top) != 0){
          newV1.y = SCREEN_HEIGHT;
          newV1.x = (int) v1.x + (SCREEN_HEIGHT - v1.y)/m;
          out1 = GenerateOutcode(newV1);
          newV1.zinv = InterpolateZinv(v1, v2, newV1);
          newV1.pos3d = Interpolate3D(v1, v2, newV1);
        }

        //Check if bottom plane needs clipping
        else if((out1 & Bottom) != 0){
         //Clip vertex to bottom plane
          newV1.y = 0;
          newV1.x = (int) v1.x + (0 - v1.y)/m;
          out1 = GenerateOutcode(newV1);
          newV1.zinv = InterpolateZinv(v1, v2, newV1);
          newV1.pos3d = Interpolate3D(v1, v2, newV1);
        }

        //Check if left plane needs clipping
        if((out1 & Left) != 0){
          //Clip Vertex to Left plane
          newV1.x = 0;
          newV1.y = (v1.y + (m * (0 - v1.x)));
          newV1.zinv = InterpolateZinv(v1, v2, newV1);
          newV1.pos3d = Interpolate3D(v1, v2, newV1);
        }

        //Check if right plane needs clipping
        else if((out1 & Right) != 0){
          //Clip Vertex to Right plane
          newV1.x = SCREEN_WIDTH;
          newV1.y = (v1.y + (m * (SCREEN_WIDTH - v1.x)));
          newV1.zinv = InterpolateZinv(v1, v2, newV1);
          newV1.pos3d = Interpolate3D(v1, v2, newV1);
        }
        //add v1 to clipped pixels
        vertices.resize(vertices.size() + 1);
        vertices[vertices.size() - 1] = newV1;
      }

      bool isV2Clipped = false;
      Pixel newV2;
      if((out2 & Top) != 0){
        //Clip vertex to top plane
        newV2.y = SCREEN_HEIGHT;
        newV2.x = (int) v2.x + (SCREEN_HEIGHT - v2.y)/m;
        out2 = GenerateOutcode(newV2);
        newV2.zinv = InterpolateZinv(v1, v2, newV2);
        newV2.pos3d = Interpolate3D(v1, v2, newV2);
        isV2Clipped = true;
      }

      //Check if bottom plane needs clipping
      else if((out2 & Bottom) != 0){
        //Clip vertex to bottom plane
        newV2.y = 0;
        newV2.x = (int) v2.x + (0 - v2.y)/m;
        out2 = GenerateOutcode(newV2);
        newV2.zinv = InterpolateZinv(v1, v2, newV2);
        newV2.pos3d = Interpolate3D(v1, v2, newV2);
        isV2Clipped = true;
      }

      //Check if left plane needs clipping
      if((out2 & Left) != 0){
        //Clip Vertex to Left plane
        newV2.x = 0;
        newV2.y = (int)(v2.y + (m * (0 - v2.x)));
        newV2.zinv = InterpolateZinv(v1, v2, newV2);
        newV2.pos3d = Interpolate3D(v1, v2, newV2);
        isV2Clipped = true;
      }

      //Check if right plane needs clipping
      else if((out2 & Right) != 0){
        //Clip Vertex to Right plane
        newV2.x = SCREEN_WIDTH;
        newV2.y = (int)(v2.y + (m * (SCREEN_WIDTH - v2.x)));
        newV2.zinv = InterpolateZinv(v1, v2, newV2);
        newV2.pos3d = Interpolate3D(v1, v2, newV2);
        isV2Clipped = true;
      }

      if(isV2Clipped){ //if v2 is clipped, add it
        vertices.resize(vertices.size() + 1);
        vertices[vertices.size() - 1] = newV2;
      }


    }
  }
  else{ //no clipping needed
    vertices.resize(vertices.size() + 1); //add the first point to vertices
    vertices[vertices.size() - 1] = v1;
  }
}

//Checks the polygon is on the screen
bool IsOnScreen( int outcodes[3] ){
  if(((outcodes[0] & Top) != 0) && ((outcodes[1] & Top) != 0) && ((outcodes[2] & Top) != 0)){
    return false;
  }
  if(((outcodes[0] & Bottom) != 0) && ((outcodes[1] & Bottom) != 0) && ((outcodes[2] & Bottom) != 0)){
    return false;
  }
  if(((outcodes[0] & Left) != 0) && ((outcodes[1] & Left) != 0) && ((outcodes[2] & Left) != 0)){
    return false;
  }
  if(((outcodes[0] & Right) != 0) && ((outcodes[1] & Right) != 0) && ((outcodes[2] & Right) != 0)){
    return false;
  }
  else{
    return true;
  }
}


bool IsCompletelyOnScreen( int outcodes[3] ){
  if(((outcodes[0]) == 0) && ((outcodes[1]) == 0) && ((outcodes[2]) == 0)){
    return true;
  }
  else return false;
}

/*
//Takes in 3 outcodes and returns which potential corner could be in the
//triangle
int WhichPotentialCorner( int outcodes[] ){
  for(int i = 0; i < 3; i++){
    // outcodes[i%3]
  }
  return 0;
}

//Takes in 3 vertices of a triangle potentially across a corner and adds it to
//returns whether the corner is within the triangle
bool CornerInTriangle( Pixel v1, Pixel v2, Pixel v3, int cornerNum ){
  //What about multiple corners???
  return false;
}
*/

int CornerCheck( Pixel v1, Pixel v2, Pixel v3 ){
  int corner = 0;

  float totalArea = (v1.x*(v2.y-v3.y) + v2.x*(v3.y-v1.y) + v3.x*(v1.y - v2.y))*0.5f;

  Pixel topLeft = {0, SCREEN_HEIGHT, 0, vec4(0,0,0,0)};
  Pixel bottomLeft = {0, 0, 0, vec4(0,0,0,0)};
  Pixel topRight = {SCREEN_WIDTH, SCREEN_HEIGHT, 0, vec4(0,0,0,0)};
  Pixel bottomRight = {SCREEN_WIDTH, 0, 0, vec4(0,0,0,0)};

  float topLeftArea = CalculateArea(v1, v2, v3, topLeft);
  float bottomLeftArea = CalculateArea(v1, v2, v3, bottomLeft);
  float topRightArea = CalculateArea(v1, v2, v3, topRight);
  float bottomRightArea = CalculateArea(v1, v2, v3, bottomRight);

  if((int) totalArea == (int) (topLeftArea)) corner = corner | TopLeft;
  if((int) totalArea == (int) (bottomLeftArea)) corner = corner | BottomLeft;
  if((int) totalArea == (int) (topRightArea)) corner = corner | TopRight;
  if((int) totalArea == (int) (bottomRightArea)) corner = corner | BottomRight;

  return corner;
}

float CalculateArea(Pixel v1, Pixel v2, Pixel v3, Pixel corner)
{
  float area1 = abs(v1.x*(v2.y-corner.y) + v2.x*(corner.y-v1.y) + corner.x*(v1.y - v2.y))*0.5f;
  float area2 = abs(v1.x*(corner.y-v3.y) + corner.x*(v3.y-v1.y) + v3.x*(v1.y - corner.y))*0.5f;
  float area3 = abs(corner.x*(v2.y-v3.y) + v2.x*(v3.y-corner.y) + v3.x*(corner.y - v2.y))*0.5f;

  return  area1 + area2 + area3;
}

//Takes in a list of vertices and modifies them to be a list of clipped vertices
//By changing and potentially adding vertices.
//Returns true if the trie was successfully clipped or false if it is not on the
//Screen
bool ClipVertices( vector<Pixel> &vertices ){
  int V = vertices.size();
  vector <Pixel> newPixels;
  int outcodes[V];

  if( V != 3 ){
    cout << "Not 3 vertices" << endl;
    return false;
  }
  else{
    for(int i=0; i<3; i++){
      outcodes[i] = GenerateOutcode(vertices[i]); //generates each outcode
    }
    if(!IsOnScreen( outcodes )) return false; //checks the polygon is on screen at all
    else if(IsCompletelyOnScreen( outcodes )) return true;
    else {
      int cornerNum = CornerCheck(vertices[0], vertices[1], vertices[2]);
      // Try to clip all the lines. Add the clipped points to the shape, remove
      // out of bounds vertices. if it contains *any number of corners*, add it.
      // Finish?
      for( int i = 0; i<3; i++ ){
        ClipLine(vertices[i], outcodes[i], vertices[(i+1)%3], outcodes[(i+1)%3], newPixels);
      }

      if((TopLeft & cornerNum) != 0) {
        //Line 1: vertex to corner
        // y1 = m1 x1 + c1
        float m1 = (vertices[0].y - SCREEN_HEIGHT)/((float)vertices[0].x - 0);
        float c1 = vertices[0].y - m1*(vertices[0].x);
        //Line 2: Other two vertexes
        // y2 = m2 x2 + c2
        float m2 = (vertices[1].y - vertices[2].y)/((float)vertices[1].x - vertices[2].x);
        float c2 = vertices[1].y - m2*(vertices[1].x);

        // m1 * x + c1 = m2 * x + c2
        int x = (int) (c2 - c1)/(m1 - m2);
        int y = (int) ((float)m1*x + c1);

        Pixel pointOnEdge = {x, y, 0, vec4(0,0,0,0)};

        pointOnEdge.zinv = InterpolateZinv(vertices[1], vertices[2], pointOnEdge);
        pointOnEdge.pos3d = Interpolate3D(vertices[1], vertices[2], pointOnEdge);

        Pixel corner = {0, SCREEN_HEIGHT, 0, vec4(0,0,0,0)};

        corner.zinv = InterpolateZinv(vertices[0], pointOnEdge, corner);
        corner.pos3d = Interpolate3D(vertices[0], pointOnEdge, corner);

        //Add pixel to shape
        bool alreadyIn = false;
        for(uint i=0; i<newPixels.size(); i++){
          if(newPixels[i].x == 0 && newPixels[i].y == SCREEN_HEIGHT) alreadyIn = true;
        }
        if(!alreadyIn){
          newPixels.resize(newPixels.size() + 1);

          newPixels[newPixels.size() - 1] = corner;
        }
      }
      if((BottomLeft & cornerNum) != 0){
        //Line 1: vertex to corner
        // y1 = m1 x1 + c1

        float m1 = (vertices[0].y - 0)/(float)(vertices[0].x - 0);
        float c1 = vertices[0].y - m1*(vertices[0].x);

        //Line 2: Other two vertexes
        // y2 = m2 x2 + c2
        float m2 = (vertices[1].y - vertices[2].y)/((float)vertices[1].x - vertices[2].x);
        float c2 = vertices[1].y - m2*(vertices[1].x);

        // m1 * x + c1 = m2 * x + c2
        int x = (int) (c2 - c1)/(m1 - m2);
        int y = (int) ((float)m1*x + c1);

        Pixel pointOnEdge = {x, y, 0, vec4(0,0,0,0)};

        pointOnEdge.zinv = InterpolateZinv(vertices[1], vertices[2], pointOnEdge);
        pointOnEdge.pos3d = Interpolate3D(vertices[1], vertices[2], pointOnEdge);

        Pixel corner = {0, 0, 0, vec4(0,0,0,0)};

        corner.zinv = InterpolateZinv(vertices[0], pointOnEdge, corner);
        corner.pos3d = Interpolate3D(vertices[0], pointOnEdge, corner);

        bool alreadyIn = false;
        for(uint i=0; i<newPixels.size(); i++){
          if(newPixels[i].x == 0 && newPixels[i].y == 0) alreadyIn = true;
        }
        if(!alreadyIn){
          newPixels.resize(newPixels.size() + 1);

          newPixels[newPixels.size() - 1] = corner;
        }
      }
      if((TopRight & cornerNum) != 0){
        //Line 1: vertex to corner
        // y1 = m1 x1 + c1
        float m1 = (vertices[0].y - SCREEN_HEIGHT)/((float)vertices[0].x - SCREEN_WIDTH);
        float c1 = vertices[0].y - m1*(vertices[0].x);
        //Line 2: Other two vertexes
        // y2 = m2 x2 + c2
        float m2 = (vertices[1].y - vertices[2].y)/((float)vertices[1].x - vertices[2].x);
        float c2 = vertices[1].y - m2*(vertices[1].x);

        // m1 * x + c1 = m2 * x + c2
        int x = (int) (c2 - c1)/(m1 - m2);
        int y = (int) ((float)m1*x + c1);

        Pixel pointOnEdge = {x, y, 0, vec4(0,0,0,0)};

        pointOnEdge.zinv = InterpolateZinv(vertices[1], vertices[2], pointOnEdge);
        pointOnEdge.pos3d = Interpolate3D(vertices[1], vertices[2], pointOnEdge);

        Pixel corner = {SCREEN_WIDTH, SCREEN_HEIGHT, 0, vec4(0,0,0,0)};

        corner.zinv = InterpolateZinv(vertices[0], pointOnEdge, corner);
        corner.pos3d = Interpolate3D(vertices[0], pointOnEdge, corner);

        //Add pixel to shape
        bool alreadyIn = false;
        for(uint i=0; i<newPixels.size(); i++){
          if(newPixels[i].x == SCREEN_WIDTH && newPixels[i].y == SCREEN_HEIGHT) alreadyIn = true;
        }
        if(!alreadyIn){
          newPixels.resize(newPixels.size() + 1);

          newPixels[newPixels.size() - 1] = corner;
        }
      }
      if((BottomRight & cornerNum) != 0){

        //Line 1: vertex to corner
        // y1 = m1 x1 + c1
        float m1 = (vertices[0].y - 0)/((float)vertices[0].x - SCREEN_WIDTH);
        float c1 = vertices[0].y - m1*(vertices[0].x);
        //Line 2: Other two vertexes
        // y2 = m2 x2 + c2
        float m2 = (vertices[1].y - vertices[2].y)/((float)vertices[1].x - vertices[2].x);
        float c2 = vertices[1].y - m2*(vertices[1].x);
        // m1 * x + c1 = m2 * x + c2
        int x = (int) (c2 - c1)/(m1 - m2);
        int y = (int) ((float)m1*x + c1);

        Pixel pointOnEdge = {x, y, 0, vec4(0,0,0,0)};

        pointOnEdge.zinv = InterpolateZinv(vertices[1], vertices[2], pointOnEdge);
        pointOnEdge.pos3d = Interpolate3D(vertices[1], vertices[2], pointOnEdge);

        Pixel corner = {SCREEN_WIDTH, 0, 0, vec4(0,0,0,0)};

        corner.zinv = InterpolateZinv(vertices[0], pointOnEdge, corner);
        corner.pos3d = Interpolate3D(vertices[0], pointOnEdge, corner);

        //Add pixel to shape
        bool alreadyIn = false;
        for(uint i=0; i<newPixels.size(); i++){
          if(newPixels[i].x == SCREEN_WIDTH && newPixels[i].y == 0) alreadyIn = true;
        }
        if(!alreadyIn){
          newPixels.resize(newPixels.size() + 1);

          newPixels[newPixels.size() - 1] = corner;
        }
      }
      vertices = newPixels;

      return true;
    }
  }
}

//Draws the depth buffer
void DrawDepth( screen* screen ){
  vector<float> foo;
  for(uint y=0; y<SCREEN_HEIGHT; y++){
    for(uint x=0; x<SCREEN_WIDTH; x++){
      foo.push_back(depthBuffer[y][x]);
    }
  }

  float max;
  max = *std::max_element(foo.begin(), foo.end());

  float bufferGrey[SCREEN_HEIGHT][SCREEN_WIDTH];
  for(uint y=0; y<SCREEN_HEIGHT; y++){
    for(uint x=0; x<SCREEN_WIDTH; x++){
      bufferGrey[y][x] = (float) depthBuffer[y][x]/max;
      vec3 color(bufferGrey[y][x], bufferGrey[y][x], bufferGrey[y][x]);
      PutPixelSDL( screen, x, y, color);
    }
  }
}

/*Place updates of parameters here*/
bool Update()
{

  SDL_Event e;
  while(SDL_PollEvent(&e))
  {
    if (e.type == SDL_QUIT)
    {
      return false;
    }
    else
    {
      if (e.type == SDL_KEYDOWN)
      {
  	    int key_code = e.key.keysym.sym;
  	    switch(key_code)
	      {
          case SDLK_UP:
            /* move camera forward */
            cameraPos.z += moveDistance;
            break;
          case SDLK_DOWN:
            /* move camera backwards */
            cameraPos.z -= moveDistance;
            break;
          case SDLK_LEFT:
            /* move camera left */
            yaw -= rotationStep;
            R[0][0] = cos(yaw);
            R[2][0] = sin(yaw);
            R[0][2] = -sin(yaw);
            R[2][2] = cos(yaw);
            break;
          case SDLK_RIGHT:
            /* move camera right */
            yaw += rotationStep;
            R[0][0] = cos(yaw);
            R[2][0] = sin(yaw);
            R[0][2] = -sin(yaw);
            R[2][2] = cos(yaw);
            break;
	        case SDLK_ESCAPE:
		        /* Move camera quit */
		          return false;
        }
  	  }
    }
  }
  return true;
}
