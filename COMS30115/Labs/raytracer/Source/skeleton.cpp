#include <iostream>
#include <glm/glm.hpp>
#include <SDL.h>
#include "SDLauxiliary.h"
#include "TestModelH.h"
#include <stdint.h>

using namespace std;
using glm::vec3;
using glm::mat3;
using glm::vec4;
using glm::mat4;

SDL_Event event;

#define SCREEN_WIDTH 320
#define SCREEN_HEIGHT 256
#define FULLSCREEN_MODE false

/* ----------------------------------------------------------------------------*/
/* FUNCTIONS                                                                   */

bool Update();
void Draw(screen* screen);

int main( int argc, char* argv[] )
{

  screen *screen = InitializeSDL( SCREEN_WIDTH, SCREEN_HEIGHT, FULLSCREEN_MODE );

  while ( Update())
    {
      Draw(screen);
      SDL_Renderframe(screen);
    }

  SDL_SaveImage( screen, "screenshot.bmp" );

  KillSDL(screen);
  return 0;
}

/*Place your drawing here*/
void Draw(screen* screen)
{
  // 1. Loop through all pixels
  // 2. Calculate ray direction.

  // 3. Call ClosestIntersection
  //    3a. If it is diffuse, calculate color
  //    3b. If it is specular bounce until diffuse is hit

  // 4. Is ClosestIntersection true
  //    4a. If TRUE, calculate color of area
  //    4b. ELSE, set pixel colour to black.

  vec4 right   ( R[0][0], R[0][1], R[0][2], 1);
  vec4 down    ( R[1][0], R[1][1], R[1][2], 1);
  vec4 forward ( R[2][0], R[2][1], R[2][2], 1);

  vec4 pos;

  for( int y = 0; y < SCREEN_HEIGHT; y++ )
  {
    for( int x = 0; x < SCREEN_WIDTH; x++ )
    {
      Intersection intersection = {pos, m, -1};

      float xDir = (x-SCREEN_WIDTH/2)*right.x + (y-SCREEN_HEIGHT/2)*down.x
             + focalLength*forward.x;
      float yDir = (x-SCREEN_WIDTH/2)*right.y + (y-SCREEN_HEIGHT/2)*down.y
             + focalLength*forward.y;
      float zDir = (x-SCREEN_WIDTH/2)*right.z + (y-SCREEN_HEIGHT/2)*down.z
             + focalLength*forward.z;

      vec4 dir = vec4(xDir, yDir, zDir, 0);

      if(ClosestIntersection( cameraPos, dir, intersection ))
      {
        vec3 totalLight = indirectLight + DirectLight( intersection );
        vec3 color = totalLight * triangles[intersection.triangleIndex].color ;
        PutPixelSDL(screen, x, y, color);
      }
      else
      {
        PutPixelSDL(screen, x, y, vec3(0.0, 0.0, 0.0));
      }
    }
    SDL_Renderframe(screen);
  }
}

/*Place your drawing here*/
void Draw(screen* screen)
{
  /* Clear buffer */
  memset(screen->buffer, 0, screen->height*screen->width*sizeof(uint32_t));

  vec3 colour(1.0,0.0,0.0);
  for(int i=0; i<1000; i++)
    {
      uint32_t x = rand() % screen->width;
      uint32_t y = rand() % screen->height;
      PutPixelSDL(screen, x, y, colour);
    }
}

/*Place updates of parameters here*/
bool Update()
{
  static int t = SDL_GetTicks();
  /* Compute frame time */
  int t2 = SDL_GetTicks();
  float dt = float(t2-t);
  t = t2;

  SDL_Event e;
  while(SDL_PollEvent(&e))
    {
      if (e.type == SDL_QUIT)
	{
	  return false;
	}
      else
	if (e.type == SDL_KEYDOWN)
	  {
	    int key_code = e.key.keysym.sym;
	    switch(key_code)
	      {
	      case SDLK_UP:
		/* Move camera forward */
		break;
	      case SDLK_DOWN:
		/* Move camera backwards */
		break;
	      case SDLK_LEFT:
		/* Move camera left */
		break;
	      case SDLK_RIGHT:
		/* Move camera right */
		break;
	      case SDLK_ESCAPE:
		/* Move camera quit */
		return false;
	      }
	  }
    }
  return true;
}


bool IsShadow( vec4 start,
               vec4 dir,
               float distance
             )
{
  //find intersections, if the intersection is closer than the given distance
  // break early and return true else return false
  // NOTE: This may change, if we do translucent objects we proabaly want the intersection
  for( Triangle triangle : triangles)
  {
    vec4 v0 = triangle.v0;
    vec4 v1 = triangle.v1;
    vec4 v2 = triangle.v2;

    vec3 e1 = vec3(v1 - v0);
    vec3 e2 = vec3(v2 - v0);
    vec3 b = vec3(start - v0);

    mat3 A( vec3(-dir), e1, e2 );
    mat3 At( vec3( b ), e1, e2 );
    mat3 Au( vec3(-dir), b, e2);
    mat3 Av( vec3(-dir), e1, b);

    float detA = glm::determinant(A);

    float detAu = glm::determinant(Au);
    float u = detAu/detA;
    if( u >= 0){
      float detAv = glm::determinant(Av);
      float v = detAv/detA;
      if( v >= 0 && u + v <= 1){
        float detAt = glm::determinant(At);
        float t = detAt/detA;
        // Checks the intersection distance is closer than the given distance
        if( t >= 0 && t < distance ){
          return true;
        }
      }
    }
  }
  return false;
}

//Change to do reflections with no lighting
vec3 DirectLight( const Intersection& i ){
  //check for shadows
  Intersection shadowI;
  float radius;
  //cast a ray from light to the point
  vec4 dir = vec4(i.position.x - lightPos.x, i.position.y-lightPos.y,
                  i.position.z-lightPos.z, 0);
  dir = glm::normalize(dir); //should be normalized

  //calculate distance to light source
  radius = sqrt(pow((lightPos.x - i.position.x), 2) + //distance from light
                pow((lightPos.y - i.position.y), 2) + //to intersection
                pow((lightPos.z - i.position.z), 2)); //point

  vec3 color = vec3(0, 0, 0);

  //FIXME Remove this shadow function, change it to check the speculairty,
  // bounce (path trace) if specular and use reflected value. Otherwise
  //calculate color using local radius.

  //Change to add slight normal rather than magic number?
  //Adjust magic number?
  if(!IsShadow(lightPos, dir, radius*0.9)){
    // p = light Color
    // n^ = normal vector
    // r^ = the direction from the surface point to the light source
    // r^ = 3d normalized vector got from the intersection point & the
    //      light position.

    float intensity;
    vec4 n = triangles[i.triangleIndex].normal;
    vec4 r = vec4(lightPos.x-i.position.x, lightPos.y-i.position.y,
                  lightPos.z-i.position.z, 0);
    r = normalize(r);
    if( 0 > dot(r, n) ) intensity = 0;            //For some reason the standard
    else intensity = dot(r, n);                   //max function wont work?

    intensity = intensity/(4*M_PI*pow(radius,2));   //set light intensity


    color = vec3( lightColor.x * intensity, lightColor.y * intensity,
                  lightColor.z * intensity );
  }
  return color;
}
