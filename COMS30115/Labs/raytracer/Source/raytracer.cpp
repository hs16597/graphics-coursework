#include <iostream>
#include <glm/glm.hpp>
#include <SDL.h>
#include "SDLauxiliary.h"
// #include "TestModelH.h" // The make file would need to be changed aswell
#include "TestModelMaterialsH.h"
#include <stdint.h>
#include <limits.h>

using namespace std;
using glm::vec3;
using glm::mat3;
using glm::vec4;
using glm::mat4;
using glm::mat4x4;

#define SCREEN_WIDTH 512
#define SCREEN_HEIGHT 512
#define FULLSCREEN_MODE false

/* ----------------------------------------------------------------------------*/
/* STRUCTS                                                                     */
struct Intersection
{
  vec4 position;
  float distance;
  int triangleIndex;
};

struct Photon
{
  float x, y, z;   // position
  vec3 wavelength;
  vec4 dir;
  vec3 power;
};

struct PhotonIntersection{
  vec4 position;
  float distance;
  int triangleIndex;
  vec4 dir;
  vec3 color; //(power)
};

struct Node
{
  float point[3];
  Photon p;
  Node *left, *right;
};

struct Node* newNode(Photon p)
{
    struct Node* temp = new Node;

    temp->point[0] = p.x;
    temp->point[1] = p.y;
    temp->point[2] = p.z;

    temp->p = p;

    temp->left = NULL;
    temp->right = NULL;
    return temp;
};

/* ----------------------------------------------------------------------------*/
/* FUNCTIONS                                                                   */

bool Update();

//ray tracing functions
int ClosestIntersection(
  vec4 start,
  vec4 dir,
  Intersection& closestIntersection);
vec3 MirrorReflect(vec3 dir, vec3 normal);

float SnellRefrac(float angleInter, float n1, float n2);
float FresnelRefl(float angleIn, float angleRefrac, float n1, float n2);

void transformationMatrix( int triangleIndex, mat4x4& t );

vec4 SpecReflect(vec4 dir, PhotonIntersection i, float variance);
vec4 DiffReflect(PhotonIntersection i);

int GenerateLightNum();
vec3 GenerateWavelength( int lightNum );
vec4 GeneratePosOnLight( int lightNum );
vec4 GenerateLightDirection( int lightNum );


//Photon Map functions
void GenerateImportanceMap(screen *iScreen);
Node *PhotonTracing( int photonNumber, Node *tree);
bool ClosestPhotonIntersect(vec4 start, vec4 dir,
                            PhotonIntersection& intersect,
                            Node** tree);
void RenderUsingPhotonMap(screen* screen, float radius, Node *tree);
void NaiveRenderPhotonMap(screen* screen, Node *root);
vec3 NaivePosToFlux( vec4 position, Node *root );
vec3 PosToFlux(Intersection intersect, vec4 viewDir, float radius, Node* root);
float LambertBRDF(vec4 dir, vec4 mormal);

// KD tree functions
Node *insertRec(Node *root, Photon p, unsigned depth);
Node *insert(Node *root, Photon p);
bool arePointsSame(Photon p1, Photon p2);
bool searchRec(Node* root, Photon p, unsigned depth);
bool search(Node* root, Photon p);
Node *neighbourRec(Node *root, Photon p, unsigned depth, float neighbourDist, Node *neighbour);
Node *neighbour(Node *root, Photon p, Node *neighbour);
Node *newNeighbourRec(Node *root, Photon p, unsigned depth, float neighbourDist, Node *neighbour, vector<Node*> neighbours);
Node *newNeighbour(Node *root, Photon p, vector<Node*> neighbours);
vector<Node*> getNeighboursWithinRadius(Node *root, Photon p, float radius);
bool inNeighbours(Photon p, vector<Node*> neighbours);
float neighbourDistance(float a[3], float b[3]);
float coordDistance(float a, float b);

/* ----------------------------------------------------------------------------*/
/* VARIABLES                                                                   */
float t = 0;
float m = std::numeric_limits<float>::max();

float focalLength = SCREEN_HEIGHT;
vec4 cameraPos( 0.0, 0.0, -4.0, 1.0 );
float MoveDistance = 0.1;
mat3 R;
float yaw;
float rotationStep = 0.2;

vector<Triangle> triangles;
vector<Sphere> spheres;

float lightMoveDistance = 0.1;
float air = 1.0f; //vaccum refractive index

//Light 1 settings: Point light in all directions
vec4 light1Pos( 0, -0.5, 0, 1.0 );
vec3 light1Color = vec3( 1, 1, 1 );
float light1Size = 0;
float light1Power = 10;

//Light 2 settings: FIXME Square light
vec4 light2Pos( -0.5, -0.99, -0.5, 0.0 );
vec4 light2Dir = light2Pos; //+ vec4(0.0f, 0.0f, 1.0f, 0.0f);
vec3 light2Color = vec3( 1, 1, 1 );
float light2size = 0.2;
float light2Power = 10;
vec4 lightDir = vec4(0.0, -1.0, 0.0, 1.0);


vec3 ambientLight = vec3(0.1, 0.1, 0.1);

/*Render Settings---------------------------------------------------------------*/
//Work in progress
int glassBounceDepth = 5; //Glass refraction is not working
bool lightTwoOn = false; //Light 2 is not working

// ---- Photon mapping settings ----
int photonNum = 10000;
float radius = 0.1;
//draws an importance map from the camera if turned on

bool lightOneOn = true;

bool render = true;
bool drawImportance = false;

// ---- Light Settings ----
bool isDirectLight = true;
bool isIndirectLight = true;
bool isAmbientLight = !isIndirectLight;


// ---- Geometry Settings ----
bool loadSpheres = true;
bool LoadPolys = true;
//If true, WASD(QE) moves the sphere in the scene instead of the light position
bool moveSphere = false;

Node *problem_root;

/* ----------------------------------------------------------------------------*/

int main( int argc, char* argv[] )
{
  screen *importanceScreen = InitializeSDL( SCREEN_WIDTH, SCREEN_HEIGHT, FULLSCREEN_MODE );
  if(!drawImportance) KillSDL(importanceScreen);

  screen *outputScreen = InitializeSDL( SCREEN_WIDTH, SCREEN_HEIGHT, FULLSCREEN_MODE );
  if(!render) KillSDL(outputScreen);
  if(LoadPolys) LoadTestModel( triangles );
  if(loadSpheres) LoadTestSpheres( spheres );

  cout << "Generating photonMap" << endl;
  Node *tree = NULL;
  if(isIndirectLight)tree = PhotonTracing( photonNum, tree );
  cout << "Finished Generating" << endl;

  if(isIndirectLight) if(tree == NULL) cout << " ----- Null Tree -----" << endl;

  R = {{1,0,0},{0,1,0},{0,0,1}};
  bool isNotQuit = true;
  while( isNotQuit )
    {
      // Draw(screen);
      if(drawImportance) GenerateImportanceMap(importanceScreen);
      if(render) RenderUsingPhotonMap(outputScreen, radius, tree);
      isNotQuit = Update();
      SDL_Renderframe(outputScreen);
    }

  SDL_SaveImage( outputScreen, "screenshot.bmp" );
  if(drawImportance)SDL_SaveImage( importanceScreen, "importanceScreen.bmp" );

  if(render)KillSDL(outputScreen);
  if(drawImportance) KillSDL(importanceScreen);
  return 0;
}

// -------------------- Helper fuctions --------------------
void PrintVec3(vec3 v){
  cout << "vec3: " << v.x << ", " << v.y << ", " << v.z << "," << endl;
}

void PrintVec4(vec4 v){
  cout << "vec4: " << v.x << ", " << v.y << ", " << v.z << "," << endl;
}

vec4 getNormal(int index, vec4 point){
  vec4 normal;
  if(index < (int) triangles.size()){
    normal = triangles[index].normal;
  }
  else{
    normal = normalize(spheres[index- (int)triangles.size()].center - point);
  }
  return normal;
}

float getSpec(int triangleIndex){
  if(triangleIndex < (int) triangles.size()) return triangles[triangleIndex].spec;
  else return spheres[triangleIndex - (int) triangles.size()].spec;
}

float getDiff(int triangleIndex){
  if(triangleIndex < (int) triangles.size()) return triangles[triangleIndex].diff;
  else return spheres[triangleIndex - (int) triangles.size()].diff;
}

bool getGlass(int triangleIndex){
  if(triangleIndex < (int) triangles.size()) return triangles[triangleIndex].glass;
  else return spheres[triangleIndex - (int) triangles.size()].glass;
}

float getRI(int triangleIndex){
  if(triangleIndex < (int) triangles.size()) return triangles[triangleIndex].ri;
  else return spheres[triangleIndex - (int) triangles.size()].ri;
}

vec3 getColor(int triangleIndex){
  vec3 color;
  if(triangleIndex < (int) triangles.size()) color = triangles[triangleIndex].color;
  else color = spheres[triangleIndex - (int) triangles.size()].color;
  return color;
}

// -------------------- Direct Light --------------------
bool IsShadow( vec4 start,
               vec4 dir,
               float distance
             )
{
  //find intersections, if the intersection is closer than the given distance
  // break early and return true else return false
  for( Triangle triangle : triangles)
  {
    vec4 v0 = triangle.v0;
    vec4 v1 = triangle.v1;
    vec4 v2 = triangle.v2;

    vec3 e1 = vec3(v1 - v0);
    vec3 e2 = vec3(v2 - v0);
    vec3 b = vec3(start - v0);

    mat3 A( vec3(-dir), e1, e2 );
    mat3 At( vec3( b ), e1, e2 );
    mat3 Au( vec3(-dir), b, e2);
    mat3 Av( vec3(-dir), e1, b);

    float detA = glm::determinant(A);

    float detAu = glm::determinant(Au);
    float u = detAu/detA;
    if( u >= 0){
      float detAv = glm::determinant(Av);
      float v = detAv/detA;
      if( v >= 0 && u + v <= 1){
        float detAt = glm::determinant(At);
        float t = detAt/detA;
        // Checks the intersection distance is closer than the given distance
        if( t >= 0 && t < distance ){
          return true;
        }
      }
    }
  }
  for( Sphere sphere : spheres){
    vec3 C = vec3(sphere.center);
    vec3 O = vec3(start);
    vec3 D = normalize(vec3(dir));
    vec3 L = C - O;

    float tca = dot(L, D);

    if(tca >= 0){ //infront of camera
      float d = sqrt(dot(L,L) - (tca*tca));
      if(d >= 0){
        float thc = sqrt((sphere.radius*sphere.radius)-(d*d));
        float t;
        if((tca - thc) < (tca + thc)) t = tca - thc;
        else t = tca + thc;
        if(t < m && t > 0){
          if(!sphere.glass)return true;
        }
      }
    }
  }
  return false;
}

//
vec3 DirectLight( const Intersection& i ){
  //check for shadows
  Intersection shadowI;
  float radius1;
  //cast a ray from light to the point
  vec4 dir = vec4(i.position.x - light1Pos.x, i.position.y-light1Pos.y,
                  i.position.z-light1Pos.z, 0);
  dir = glm::normalize(dir); //should be normalized

  //calculate distance to light source
  radius1 = sqrt(pow((light1Pos.x - i.position.x), 2) + //distance from light
                pow((light1Pos.y - i.position.y), 2) + //to intersection
                pow((light1Pos.z - i.position.z), 2)); //point

  vec3 color = vec3(0, 0, 0);

  vec3 ambientLightVector;
  if(isAmbientLight) ambientLightVector = ambientLight;
  else ambientLight = vec3(0,0,0);

  if(!IsShadow(light1Pos, dir, radius1*0.9)){
    // p = light Color
    // n^ = normal vector
    // r^ = the direction from the surface point to the light source
    // r^ = 3d normalized vector got from the intersection point & the
    //      light position.

    float intensity;
    vec4 n = getNormal(i.triangleIndex, i.position );
    vec4 r = vec4(light1Pos.x-i.position.x, light1Pos.y-i.position.y,
                  light1Pos.z-i.position.z, 0);
    r = normalize(r);
    if( 0 > dot(r, n) ) intensity = 0;               //For some reason
    else intensity = dot(r, n);                      //max function won't work?

    intensity = intensity/(4*M_PI*pow(radius1,2));   //set light intensity

    intensity = intensity * light1Power;

    color = vec3( light1Color.x * intensity, light1Color.y * intensity,
                  light1Color.z * intensity );

    color = intensity * getColor(i.triangleIndex);
  }
  return color+ambientLightVector;
}

int ClosestIntersection(
  vec4 start,
  vec4 dir,
  Intersection& closestIntersection)
{
  int intrFound = 0;
  closestIntersection.distance = m;
  int index = 0;
  for( Triangle triangle : triangles)
  {
    vec4 v0 = triangle.v0;
    vec4 v1 = triangle.v1;
    vec4 v2 = triangle.v2;

    vec3 e1 = vec3(v1 - v0);
    vec3 e2 = vec3(v2 - v0);
    vec3 b = vec3(start - v0);

    mat3 A( vec3(-dir), e1, e2 );
    mat3 At( vec3( b ), e1, e2 );
    mat3 Au( vec3(-dir), b, e2);
    mat3 Av( vec3(-dir), e1, b);

    float detA = glm::determinant(A);

      float detAu = glm::determinant(Au);
      float u = detAu/detA;
      if( u >= 0){
        float detAv = glm::determinant(Av);
        float v = detAv/detA;
        if( v >= 0 && u + v <= 1){
          float detAt = glm::determinant(At);
          float t = detAt/detA;
          if( t >= 0 ){
          // Check against inequalities to see if it's inside the triangle
          // Closest intersection = intersection that is the least distance from
          // the start of the ray.
          // Calculate distance value, compare with previous distance. If less,
          // calculate position and triangle index.
          if( t < closestIntersection.distance )
          {
            intrFound = 1;
            closestIntersection.position = start + t * dir + 0.001f*triangle.normal;
            closestIntersection.distance = t;
            closestIntersection.triangleIndex = index;
          }
        }
      }
    }
    index++;
  }

  for (Sphere sphere : spheres){
    vec3 C = vec3(sphere.center);
    vec3 O = vec3(start);
    vec3 D = normalize(vec3(dir));
    vec3 L = C - O;

    float tca = dot(L, D);

    if(tca >= 0){ //infront of camera
      float d = sqrt(dot(L,L) - (tca*tca));
      if(d >= 0){
        float thc = sqrt((sphere.radius*sphere.radius)-(d*d));
        float t;
        if(((tca - thc) < (tca + thc)) && ((tca - thc) > 0)) t = tca - thc;
        else t = tca + thc;
        if(t < closestIntersection.distance && t > 0){
          intrFound = 1;
          closestIntersection.position = start + t * vec4(D,0) + 0.001f*getNormal(index, start + (t * vec4(D,0)));
          closestIntersection.distance = t;
          closestIntersection.triangleIndex = index;
        }
      }
    }
    index++;
  }
  //check no intersection
  if(!intrFound) {
    closestIntersection.triangleIndex = -1;
  }
  else{
    //Check specularity
    float spec = getSpec(closestIntersection.triangleIndex);
    bool glass = getGlass(closestIntersection.triangleIndex);
    if(glass){
      return 2;
      //Becomes made up of proportion of reflected & proportion refracted
      // Need to limit number of bounces by passing depth
      // Problem, can't return the two rays, need to move this to somewhere else!
    }
    else {
      if( spec == 1.0f){
        vec3 tempDir1 = vec3(dir);
        vec3 tempDir2 = normalize(tempDir1);
        vec3 normal1 = vec3(getNormal(closestIntersection.triangleIndex, closestIntersection.position));
        vec3 normal2 = normalize(normal1);
        vec3 tempDir = MirrorReflect(tempDir2,normal2);
        vec4 newDir = vec4(tempDir, 0.0f);

        Intersection newIntersect;
        //Shoot new reflected ray
        intrFound = ClosestIntersection(closestIntersection.position, newDir, newIntersect);

        closestIntersection = newIntersect;
      }
    }
  }

  return intrFound;
}

// Calculates mirror reflection direction
vec3 MirrorReflect(vec3 dir, vec3 normal){
  return dir - 2.0f * dot(dir, normal) * normal;
}

// calculates the angle of refraction
float SnellRefrac(float angleIn, float n1, float n2){
  float refrac = n1 * sin(angleIn)/n2;  //Check floating point errors
  if(refrac >= 1) return 1;             //for total Internal refraction
  else return asin(refrac);
}

// Takes in the angle of incidence and the angle of refraction and returns the
// proportion of ray which is reflected
float FresnelRefl(float angleIn, float angleRefrac, float n1, float n2){
  float Fp = pow((n2 * cos(angleIn) - n1 * cos(angleRefrac))/
                 (n2 * cos(angleIn) + n1 * cos(angleRefrac)),2);
  float Fs = pow((n1 * cos(angleRefrac) - n2 * cos(angleIn))/
                 (n1 * cos(angleRefrac) + n2 * cos(angleIn)),2);
  return 0.5f * (Fp + Fs);
}


// ---------------------- Photon mapping functions ----------------------
//fills the empty matrix passed in with a transformation matrix
void transformMatrix( PhotonIntersection i, mat4x4& t ){
  //get an orthogonal vector
  // tangent is equivalent to one side of polygon, calculate the bi-tangent
  vec3 normal = vec3(getNormal(i.triangleIndex, i.position));                                //normal
  vec3 tangent = vec3(triangles[i.triangleIndex].v0) - vec3(triangles[i.triangleIndex].v1);      //tangent
  vec3 bitangent =  glm::cross(normal, tangent);                                             //binormal
  t = {tangent.x,   tangent.y,   tangent.z,   0,
       normal.x,    normal.y,    normal.z,    0,
       bitangent.x, bitangent.y, bitangent.z, 0,
       0,           0,           0,           1
     };

}

//Given a normal, a reflected angle and a varience (roughness) returns a
//reflected direction. FIXME Should be done using BRDF sampling
vec4 SpecReflect(vec4 dir, PhotonIntersection i, float variance){
  //calculate perfect reflection angle

  vec3 vec3dir = vec3(dir);
  vec3 normal = vec3(getNormal(i.triangleIndex, i.position));

  vec3 reflect = MirrorReflect(vec3dir, normal);

  return vec4(reflect.x, reflect.y, reflect.z, 0);
}

// https://www.scratchapixel.com/lessons/mathematics-physics-for-computer-graphics/geometry/spherical-coordinates-and-trigonometric-functions
// https://www.scratchapixel.com/lessons/mathematics-physics-for-computer-graphics/mathematics-of-
// https://www.scratchapixel.com/lessons/mathematics-physics-for-computer-graphics/geometry/creating-an-orientation-matrix-or-local-coordinate-system
//Given a normal returns a random angle for the diffuse reflection.
vec4 DiffReflect(PhotonIntersection i){
  //Generate random theta within range (0 to pi/2)
  float theta = M_PI * (((float) (rand())) / (float) (RAND_MAX));
  //Generate random phi within range (0 to 2pi)
  float phi = M_PI*(((float) (rand())) / (float) (RAND_MAX));

  //transfrom spherical co-ords to local cartesian
  vec4 randomReflect = vec4(cos(phi) * sin(theta), sin(phi) * sin(theta), cos(theta), 0);

  //transform local cartesian co-ords to world
  mat4x4 t;
  transformMatrix(i, t);
  if(i.triangleIndex < (int) triangles.size()){
    randomReflect = randomReflect * inverse(t);
  }
  //Cannot handle spherical diffuse reflections right now FIXME
  else cout << "----- DIFFUSE SPHERE REFLECT -----" << endl; //FIXME Can just rotate normal 90*?
  return randomReflect;
}

//FIXME Refraction does not work fully
vec3 DirectGlassLight( Intersection i, vec4 dir, int depth, bool inObject){
  if(depth > glassBounceDepth) return vec3(0,0,0);
  //find out the angles of reflection/refraction
  float n1;
  float n2;
  if(!inObject){
    n1 = air;
    n2 = getRI(i.triangleIndex);
  }
  else {
    n1 = getRI(i.triangleIndex);
    n2 = air;
  }
  vec3 I = normalize(vec3(dir));
  vec3 N = normalize(vec3(getNormal(i.triangleIndex, i.position)));

  float c1 = dot(I, N);

  //calculate the amount of light reflecting/refracting
  float R0 = pow(((n1-n2)/(n1+n2)),2);
  float reflectProp = R0 + (1 - R0)* pow((1 - c1),5);

  //clamp value -- Needed?
  if(reflectProp < 0) reflectProp = 0;
  if (reflectProp > 1) reflectProp = 1;

  float refractProp = 1.0 - reflectProp;

  float n = n1/n2;
  //Clamped to avoid errors nan errors
  float c2 = (1 - (pow(n, 2) * (1 - pow(c1,2)))) < 0 ? 0 : sqrt(1 - (pow(n, 2) * (1 - pow(c1,2))));
  vec3 T = n*I + (((n*c1) - c2) * N);
  vec4 refractDir = vec4(T, 0);

  vec3 reflectCol;
  vec3 refractCol;

  Intersection reflectI;
  Intersection refractI;

  vec4 reflectDir = vec4(MirrorReflect(I, N), 0);

  int reflectIType = ClosestIntersection(i.position, reflectDir, reflectI);                                                   //shoot reflection ray from intersection in reflection direction
  int refractIType = ClosestIntersection(i.position - 0.002f*getNormal(i.triangleIndex, i.position), refractDir, refractI);   //Shoot refraction ray

  if(reflectIType == 0) reflectCol = vec3(0,0,0);                                                           //Misses any geometry
  else if(reflectIType == 2) reflectCol = DirectGlassLight( reflectI, reflectDir, depth+1, !inObject );     //Hits glass
  else reflectCol = DirectLight(reflectI);                                                                  //Hits geometry

  if(refractIType == 0) {refractCol = vec3(0,0,0);}// cout << "nothing hit" << endl;}                                                           //Misses any geometry
  else if(refractIType == 2) refractCol = DirectGlassLight( refractI, refractDir, depth+1, !inObject );     //Hits glass
  else {refractCol = DirectLight(refractI);}                                                                //Hits geometry
  return (reflectProp * reflectCol) + (refractProp * refractCol);
}

//take "image" from light position. Fire rays and store if object hit is
//caustic, volumetric or geometric. Use this to bias photon firing. Fire more
//photons into caustics/ volumetrics.
void GenerateImportanceMap(screen* iScreen){

  vec4 right   ( R[0][0], R[0][1], R[0][2], 1);
  vec4 down    ( R[1][0], R[1][1], R[1][2], 1);
  vec4 forward ( R[2][0], R[2][1], R[2][2], 1);

  vec4 pos;
  for(int x = 0; x < SCREEN_WIDTH; x++){
    for(int y = 0; y < SCREEN_HEIGHT; y++){

      Intersection intersection = {pos, m, -1};

      float xDir = (x-SCREEN_WIDTH/2)*right.x + (y-SCREEN_HEIGHT/2)*down.x
             + focalLength/2*forward.x;
      float yDir = (x-SCREEN_WIDTH/2)*right.y + (y-SCREEN_HEIGHT/2)*down.y
             + focalLength/2*forward.y;
      float zDir = (x-SCREEN_WIDTH/2)*right.z + (y-SCREEN_HEIGHT/2)*down.z
             + focalLength/2*forward.z;

      vec4 dir = normalize(vec4(xDir, zDir, yDir, 0));

      //shoot rays from the light into the scene
      int iType = ClosestIntersection( light1Pos, dir, intersection);
      if( iType == 0){ //if it hits nothing return 0
        PutPixelSDL(iScreen, x, y, vec3(0,0,0));
      }
      if ( iType == 2 ){
        //Glass reflection
        PutPixelSDL(iScreen, x, y, vec3(0,0,0));
      }
      else if (iType == 1){
        //Draw for the moment, should be put in a data structure to query for
        //photon firing
        vec3 pixelColor;
        if(intersection.triangleIndex >= (int) triangles.size()) pixelColor = vec3(1,1,1);
        else pixelColor = DirectLight(intersection) * triangles[intersection.triangleIndex].color;
        PutPixelSDL(iScreen, x, y, pixelColor);
      }
    }
    SDL_Renderframe(iScreen);
  }
}

//Calculate where the photon terminates using russian roulette reflections.
//Only does Diffuse reflections
bool ClosestPhotonIntersect(vec4 start, vec4 dir, PhotonIntersection& intersect,
                            float flux, Node** tree){
  bool intrFound = false;
  intersect.distance = m;
  int index = 0;

  for( Triangle triangle : triangles)
  {
    vec4 v0 = triangle.v0;
    vec4 v1 = triangle.v1;
    vec4 v2 = triangle.v2;

    vec3 e1 = vec3(v1 - v0);
    vec3 e2 = vec3(v2 - v0);
    vec3 b = vec3(start - v0);

    mat3 A( vec3(-dir), e1, e2 );
    mat3 At( vec3( b ), e1, e2 );
    mat3 Au( vec3(-dir), b, e2);
    mat3 Av( vec3(-dir), e1, b);

    float detA = glm::determinant(A);

      float detAu = glm::determinant(Au);
      float u = detAu/detA;
      if( u >= 0){
        float detAv = glm::determinant(Av);
        float v = detAv/detA;
        if( v >= 0 && u + v <= 1){
          float detAt = glm::determinant(At);
          float t = detAt/detA;
          if( t >= 0 ){
          if( t < intersect.distance )
          {
            intrFound = true;
            intersect.position = start + t * dir + 0.001f*triangle.normal;
            intersect.distance = t;
            intersect.triangleIndex = index;
            intersect.dir = dir;
          }
        }
      }
    }
    index++;
  }
  for (Sphere sphere : spheres){
    vec3 C = vec3(sphere.center);
    vec3 O = vec3(start);
    vec3 D = normalize(vec3(dir));
    vec3 L = C - O;

    float tca = dot(L, D);

    if(tca >= 0){ //infront of camera
      float d = sqrt(dot(L,L) - (tca*tca));
      if(d >= 0){
        float thc = sqrt((sphere.radius*sphere.radius)-(d*d));
        float t;
        if(((tca - thc) < (tca + thc)) && ((tca - thc) > 0)) t = tca - thc;
        else t = tca + thc;
        if(t < intersect.distance && t > 0){
          intrFound = 1;
          intersect.position = start + t * vec4(D,0) + 0.001f*getNormal(index, start + (t * vec4(D,0)));
          intersect.distance = t;
          intersect.triangleIndex = index;
        }
      }
    }
    index++;
  }
  if(!intrFound) { //check no intersection
    intersect.triangleIndex = -1;
  }
  else{
    float diff = getDiff(intersect.triangleIndex);
    float spec = getSpec(intersect.triangleIndex);
    if(spec == 1.0f){ //Checks for perfectly specular
      vec3 dir1 = normalize(vec3(dir));
      vec3 normal1 = normalize(vec3(getNormal(intersect.triangleIndex,intersect.position)));
      vec3 tempDir = MirrorReflect(dir1,normal1);
      vec4 newDir = vec4(tempDir, 1.0f);

      PhotonIntersection newIntersect;
      //Shoot new reflected ray
      intrFound = ClosestPhotonIntersect(intersect.position, newDir, newIntersect, flux, tree);
      intersect = newIntersect;
    }
    else{
      float r1 = ((float) (rand())) / (float) (RAND_MAX);
      if(r1 <= diff){ //diffuse reflect
        vec4 newDir = DiffReflect(intersect);
        vec4 normal = normalize(getNormal(intersect.triangleIndex, intersect.position));
        float dotProd = dot(newDir, normal);
        dotProd <= 0 ? cout << "Negative dotProd" << endl : true;
        PhotonIntersection newIntersect;
        newIntersect.color = triangles[intersect.triangleIndex].color;

        //Add Copy to tree
        vec3 wavelength = vec3(0,0,0);
        vec3 power = intersect.color * flux;
        Photon p = {intersect.position.x,intersect.position.y,intersect.position.z,
          wavelength, intersect.dir, power};
        //adds photon to data structure
        *tree = insert(*tree, p); // Change this to be in the closestPhotonIntersect part

        intrFound = ClosestPhotonIntersect(intersect.position, newDir, newIntersect, flux, tree);
        intersect = newIntersect;
      }
      else if(r1 <= diff + spec){
        //Specular reflection FIXME: needs to be done using BRDF sampling

        vec4 newDir = SpecReflect(dir, intersect, 0.0f);
        PhotonIntersection newIntersect;
        newIntersect.color = triangles[intersect.triangleIndex].color;
        intrFound = ClosestPhotonIntersect(intersect.position, newDir, newIntersect, flux, tree);
        intersect = newIntersect;
      }
      else{
        //else absorbed
        //Adds it to the tree
        vec3 wavelength = vec3(0,0,0);
        vec3 power = intersect.color * flux;
        Photon p = {intersect.position.x,intersect.position.y,intersect.position.z,
          wavelength, intersect.dir, power};

        //adds photon to data structure
        *tree = insert(*tree, p);
      }
    }
  }
  return intrFound;
}

//change this to use N light sources
int GenerateLightNum(){
  if(lightOneOn && lightTwoOn){
    //Generate a random number, return dependent on lights power
    float r1 = (light1Power + light2Power) * (((float) (rand())) / (float) (RAND_MAX));
    if (r1 > light1Power) return 1;
    else return 2;
  }
  else if(lightOneOn){
    return 1;
  }
  else if(lightTwoOn){
    return 2;
  }
  else return -1;
}

//FIXME Ran out of time
vec3 GenerateWavelength( int lightNum ){
  return -1.0f*vec3(1,1,1);
}

//Change to light type
vec4 GeneratePosOnLight( int lightNum ){
  if(lightNum == 1){
    return light1Pos;
  }
  else{
    // float r1 = 0.0f; //(((float) (rand())) / (float) (RAND_MAX))/10.0f - 1.0f;
    // float r2 = 0.0f; //(((float) (rand())) / (float) (RAND_MAX))/10.0f - 1.0f;
    return light2Pos;
  }

}

vec4 GenerateLightDirection( int lightNum ){
  if(lightNum == 1){ //point light, all directions
    //Generate random theta within range (0 to pi)
    float phi = M_PI * (((float) (rand())) / (float) (RAND_MAX));
    //Generate random phi within range (0 to 2pi)
    float theta = 2 * M_PI * (((float) (rand())) / (float) (RAND_MAX));

    //transfrom spherical co-ords to local cartesian
    vec4 randomDir = vec4(cos(phi) * sin(theta), sin(phi) * sin(theta), cos(theta), 0);

    return normalize(randomDir);
  }
  else{ //square light - FIXME Just broken, ran out of time
    float r1 = (4.0f*((float) (rand())) / (float) (RAND_MAX)) - 2.0f;
    float r2 = (4.0f*((float) (rand())) / (float) (RAND_MAX)) - 2.0f;
    //get a normalized direction vector
    vec4 direction = normalize(vec4(r1, 1.0f, r2, 0.0f));
    return direction;
  }
}

//Generates a photon map with N photons in it
Node *PhotonTracing( int photonNum, Node* tree){
  //do until enough photons
  for(int i = 0; i<photonNum; i++){ //repeat until photon map full? Can be until photonMap.size()
    int debug = 0;
    //Choose a light
    int lightNum = GenerateLightNum();
    float flux;
    if(lightNum == 1) flux = light1Power/(float) photonNum;
    else if(lightNum == 2) flux = light2Power/(float) photonNum;
    else cout << "Light_num error" << endl;

    PhotonIntersection intersect;
    intersect.color = vec3(1,1,1);
    vec4 photonStart;
    vec4 photonDir;
    //Trace photon
    bool hasIntersected = false;
    while(!hasIntersected && (debug<1000)){                                     //keeps shooting the photons ray until it intersects
      // cout << "Tracing ray: " << i << endl;
      photonStart = GeneratePosOnLight( lightNum );
      photonDir = GenerateLightDirection( lightNum );
      hasIntersected = ClosestPhotonIntersect(light1Pos, photonDir, intersect, flux, &tree);
      debug++;
      if(!hasIntersected) i++;                                                  //if the photon goes of the edge reduce light power and move on
    }
    if(debug == 1000){                                                          //if it goes of the screen 1000 times in a row, "exit early"
       cout << " -------- PHOTON TRACING ERROR --------- " << endl;
       Node *error = NULL;
       cout << error->p.wavelength.x << endl;                                   //crashes (seg fault) so don't have to wait
     }
  }
  return tree;
}

//returns closest neighbour
vec3 NaivePosToFlux(vec4 position, Node *root){
  //consult the data tree
  Photon p = {position.x, position.y, position.z, vec3(-1,-1,-1), vec4(0,0,0,0), vec3(0,0,0)}; //position.x, position.y, position.z

  Node* nearest = neighbour(root, p, NULL);
  vec3 color = nearest->p.power;

  float distance = sqrt(pow((nearest->p.x - position.x),2) +
                        pow((nearest->p.y - position.y),2) +
                        pow((nearest->p.z - position.z),2));
  //return the relevant value
  if(distance > radius) return vec3(0.0f, 0.0f, 0.0f);
  else return color;
}

//Renders the photonMap without smoothing
void NaiveRenderPhotonMap(screen* screen, Node *root){ //bool refelctions
  vec4 right   ( R[0][0], R[0][1], R[0][2], 1);
  vec4 down    ( R[1][0], R[1][1], R[1][2], 1);
  vec4 forward ( R[2][0], R[2][1], R[2][2], 1);

  vec4 pos;
  //for every pixel
  for(int x=0; x<SCREEN_WIDTH; x++){
    for(int y=0; y<SCREEN_HEIGHT; y++){
      //shoot a ray
      Intersection intersection = {pos, m, -1};

      float xDir = (x-SCREEN_WIDTH/2)*right.x + (y-SCREEN_HEIGHT/2)*down.x
             + focalLength*forward.x;
      float yDir = (x-SCREEN_WIDTH/2)*right.y + (y-SCREEN_HEIGHT/2)*down.y
             + focalLength*forward.y;
      float zDir = (x-SCREEN_WIDTH/2)*right.z + (y-SCREEN_HEIGHT/2)*down.z
             + focalLength*forward.z;

      vec4 dir = vec4(xDir, yDir, zDir, 0);
      dir = normalize(dir);

      //see where the ray hits
      if(!ClosestIntersection( cameraPos, dir, intersection )){
        PutPixelSDL(screen, x, y, vec3(0,0,0));
      }
      else{
        //Set pixel values to whatever it hits from photonMap
        vec3 pixelColor = NaivePosToFlux(intersection.position, root);
        PutPixelSDL(screen, x, y, pixelColor);
      }
    }
    SDL_Renderframe(screen);
  }
}

//diffuse BRDF
float LambertBRDF(vec4 dir, vec4 normal){
  float power = glm::dot(dir, normal);
  if (power < 0) power = 0;
  return power;
}

//Specular BRDF TODO
vec3 PhongBRDF(vec3 Dir, int triIndex, vec3 viewDir){
  // k_d = triangles[triIndex].diff;
  // k_S = triangles[triIndex].spec;
  //
  // vec3 R = MirrorReflect(vec3(Dir), trianglestriIndex].normal);
  //
  // i_s = ?
  // vec3 I = k_d * (dot (Dir, triangles[triIndex].normal) * i_d) + k_s (dot(R, viewDir) * i_s);
  return vec3(0,0,0);
}

vec3 PosToFlux(Intersection intersect, vec4 viewDir, float radius, Node* root){
  vec3 total = vec3(0,0,0);
  //Get neighbours in radius
  Photon p = {intersect.position.x,intersect.position.y,intersect.position.z,
              vec3(0,0,0), vec4(0,0,0,0), vec3(0,0,0)};
  vector<Node*> neighbours;

  neighbours = getNeighboursWithinRadius(root, p, radius);

  for(uint i = 0; i < neighbours.size(); i++){
    //  Calculate outgoing Radiance Using BRDF, sum up photon contributions
    float contribution = LambertBRDF(-neighbours[i]->p.dir, getNormal(intersect.triangleIndex, intersect.position)); //intensity given direction
    total = total + (contribution * neighbours[i]->p.power); //Colored light
  }
  total = total / (float) (M_PI * pow(radius,2));
  total = total;

  return total;
}


void RenderUsingPhotonMap(screen* screen, float radius, Node* root){ //TODO add importance for caustics
  //If hits a diffuse surface -> average value of photons in the area.
  vec4 right   ( R[0][0], R[0][1], R[0][2], 1);
  vec4 down    ( R[1][0], R[1][1], R[1][2], 1);
  vec4 forward ( R[2][0], R[2][1], R[2][2], 1);

  vec4 pos;
  //for every pixel
  for(int x=0; x<SCREEN_WIDTH; x++){
    for(int y=0; y<SCREEN_HEIGHT; y++){
      //shoot a ray
      Intersection intersection = {pos, m, -1};

      float xDir = (x-SCREEN_WIDTH/2)*right.x + (y-SCREEN_HEIGHT/2)*down.x
             + focalLength*forward.x;
      float yDir = (x-SCREEN_WIDTH/2)*right.y + (y-SCREEN_HEIGHT/2)*down.y
             + focalLength*forward.y;
      float zDir = (x-SCREEN_WIDTH/2)*right.z + (y-SCREEN_HEIGHT/2)*down.z
             + focalLength*forward.z;

      vec4 dir = normalize(vec4(xDir, yDir, zDir, 0));

      //see where the ray hits
      int iType = ClosestIntersection( cameraPos, dir, intersection );
      if(iType == 0){
        PutPixelSDL(screen, x, y, vec3(0,0,0)); //misses all geometry
      }
      else if(iType == 2){                      //glass hit
        vec3 pixelColor = DirectGlassLight(intersection, normalize(dir), 0, false);
        PutPixelSDL(screen, x, y, pixelColor);
      }
      else if(iType == 1){                      //Regular hit
        //Set pixel values to whatever it hits from photonMap
        vec3 indirectLight;
        vec3 directLightVector;

        if(isDirectLight)directLightVector = DirectLight(intersection) * getColor(intersection.triangleIndex);
        else directLightVector = vec3(0,0,0);

        if(isIndirectLight) indirectLight = PosToFlux(intersection, dir, radius, root) * getColor(intersection.triangleIndex);
        else indirectLight = vec3(0,0,0);

        PutPixelSDL(screen, x, y, directLightVector + indirectLight);
      }
    }
    SDL_Renderframe(screen);
  }
}

/* ============== Photon map scattering ==============
  Can shoot rays of different wavelength, we know how they scatter,
  just need to shoot more photons.
  - Easiest to implement
  - Need to figure out how to propper reflections

  Similar to a pathtracer for each photon losing energy as it bounces
  - most difficult

  Could split up the photon when it hits a refractive surface,
  find max and min values and distribute across.
  - Would need to discretise world before
  - Sound cool
*/

/* ===== Useful links =====
Thesis: https://cs.dartmouth.edu/~wjarosz/publications/dissertation/chapter7.pdf
Henrik Jensen Siggraph Tutorial: https://graphics.stanford.edu/courses/cs348b-00/course8.pdf
University of Swansea: http://cs.swansea.ac.uk/visualcomputing/?p=283 (Not useful but has the cool picture)

Photon mapping synopsis: http://marctenbosch.com/photon/
Photon mapping in a nutshell: http://ispm.philipshield.com/photon-mapping-in-a-nutshell/

Importance sampling of a BRDF: https://computergraphics.stackexchange.com/questions/4979/what-is-importance-sampling

subreddit: https://www.reddit.com/r/coms30115/
subreddit photon mapping stuff: https://www.reddit.com/r/coms30115/comments/bbnxf7/photon_mapping_resources/

Kd-tree: https://www.geeksforgeeks.org/k-dimensional-tree/
*/

// pointer->whatever  object.whatever

// -------------------- KD-tree --------------------
// Inserts a new node and returns root of modified tree
// The parameter depth is used to decide axis of comparison
Node *insertRec(Node *root, Photon p, unsigned depth)
{
  // Tree is empty

  if (root == NULL) {
    Node *temp = newNode(p);
    return temp;
  }

  // Calculate current dimension (cd) of comparison
  unsigned cd = depth % 3;
  float point[3] = {p.x, p.y, p.z};

  // Compare the new point with root on current dimension 'cd'
  // and decide the left or right subtree
  if (point[cd] < (root->point[cd])){
    root->left = insertRec(root->left, p, depth + 1);
  }
  else
  {
    root->right = insertRec(root->right, p, depth + 1);
  }
  return root;
}

// Function to insert a new point with given point in
// KD Tree and return new root. It mainly uses above recursive
// function "insertRec()"
Node *insert(Node *root, Photon p)
{
  Node *temp = insertRec(root, p, 0);
  return temp;
}

// A utility method to determine if two Points are same in K Dimensional space,
// only checks space doesn't check attributes of the photon
bool arePointsSame(Photon p1, Photon p2)
{
  // Compare individual pointinate values
  return ((p1.x == p2.x) && (p1.y == p2.y) && (p1.z == p2.z));
}

// Searches a Point represented by "point[]" in the K D tree.
// The parameter depth is used to determine current axis.
bool searchRec(Node* root, Photon p, unsigned depth)
{
  // Base cases
  if (root == NULL) return false;

  if (arePointsSame(root->p, p)) return true;

  // Current dimension is computed using current depth and total
  // dimensions (k)
  unsigned cd = depth % 3;
  float point[3] = {p.x, p.y, p.z};

  // Compare point with root with respect to cd (Current dimension)
  if (point[cd] < root->point[cd]) {
    return searchRec(root->left, p, depth + 1);
  }

  return searchRec(root->right, p, depth + 1);
}

// Searches a Point in the K D tree. It mainly uses
// searchRec()
bool search(Node* root, Photon p)
{
  // Pass current depth as 0
  return searchRec(root, p, 0);
}

//nearest neighbour search: https://en.wikipedia.org/wiki/K-d_tree
//1. Start with root, search left or right similar to insertion
//2. Once it reaches a leaf node it checks that node point and if the distance
//   is better that node is stored as current best
//3. The algorithm unwidns the reccursion of the tree, performing the following
//   steps at each Node
//    3a. if the node is closer than the current best, it becomes current best
//    3b. The algorithm checks whether there could be any points on the other
//        side of the splitting plane that are closer to the search point
//        (Create a sphere, see if there COULD be a better solution in other
//        branch).
//        3b. True: Needs to move down the other branch
//        3b. False: Can continue walking up branch

Node *neighbourRec(Node* root, Photon p, unsigned depth, float neighbourDist, Node* neighbour)
{
  // Current dimension is computed using current depth and total
  // dimensions (k)
  unsigned cd = depth % 3; // k = 3
  float point[3] = {p.x, p.y, p.z};
  float dist = 0;

  if(root == NULL) return neighbour;

  if(point[cd] < root->point[cd])
  {
    neighbour = neighbourRec(root->left, p, depth+1, neighbourDist, neighbour);
    if(neighbour == NULL)
    {
      neighbour = root;
    }
    neighbourDist = neighbourDistance(neighbour->point, point);
    dist = neighbourDistance(root->point, point);
    if(dist < neighbourDist)
    {
      neighbour = root;
      neighbourDist = dist;
    }
    if(root->right != NULL)
    {
      float splitDist = coordDistance(root->point[cd], point[cd]);
      if(splitDist < neighbourDist)
      {
        neighbour = neighbourRec(root->right, p, depth+1, neighbourDist, neighbour);
      }
    }
  }
  else if(point[cd] >= root->point[cd])
  {
    neighbour = neighbourRec(root->right, p, depth+1, neighbourDist, neighbour);
    if(neighbour == NULL)
    {
      neighbour = root;
    }
    neighbourDist = neighbourDistance(neighbour->point, point);
    dist = neighbourDistance(root->point, point);
    if(dist < neighbourDist)
    {
      neighbour = root;
      neighbourDist = dist;
    }
    if(root->left != NULL)
    {
      float splitDist = coordDistance(root->point[cd], point[cd]);
      if(splitDist < neighbourDist)
      {
        neighbour = neighbourRec(root->left, p, depth+1, neighbourDist, neighbour);
      }
    }
  }

  return neighbour;
}

Node *neighbour(Node* root, Photon p, Node* neighbour)
{
  // Starting depth is zero. Starting can be 0 as it gets written when leaf is reached
  return neighbourRec(root, p, 0, 0, neighbour);
}

Node *newNeighbourRec(Node *root, Photon p, unsigned depth, float neighbourDist, Node *neighbour, vector<Node*> neighbours)
{
  unsigned cd = depth % 3; //k = 3
  float dist = 0;
  float point[3] = {p.x, p.y, p.z};

  if(root == NULL) return neighbour;

  if(point[cd] < root->point[cd])
  {
    neighbour = newNeighbourRec(root->left, p, depth+1, neighbourDist, neighbour, neighbours);
    if(neighbour == NULL && !inNeighbours(root->p, neighbours))
    {
      neighbour = root;
    }
    dist = neighbourDistance(root->point, point);
    if(neighbour == NULL)
    {
      neighbourDist = numeric_limits<float>::max();
    }else{
      neighbourDist = neighbourDistance(neighbour->point, point);
    }
    if(dist < neighbourDist && !inNeighbours(root->p, neighbours))
    {
      neighbour = root;
      neighbourDist = dist;
    }
    if(root->right != NULL)
    {
      float splitDist = coordDistance(root->point[cd], point[cd]);
      if(splitDist < neighbourDist)
      {
        neighbour = newNeighbourRec(root->right, p, depth+1, neighbourDist, neighbour, neighbours);
      }
    }
  }
  else if(point[cd] >= root->point[cd])
  {
    neighbour = newNeighbourRec(root->right, p, depth+1, neighbourDist, neighbour, neighbours);
    if(neighbour == NULL && !inNeighbours(root->p, neighbours))
    {
      neighbour = root;
    }
    dist = neighbourDistance(root->point, point);
    if(neighbour == NULL)
    {
      neighbourDist = numeric_limits<float>::max();
    }else{
      neighbourDist = neighbourDistance(neighbour->point, point);
    }
    if(dist < neighbourDist && !inNeighbours(root->p, neighbours))
    {
      neighbour = root;
      neighbourDist = dist;
    }
    if(root->left != NULL)
    {
      float splitDist = coordDistance(root->point[cd], point[cd]);
      if(splitDist < neighbourDist)
      {
        neighbour = newNeighbourRec(root->left, p, depth+1, neighbourDist, neighbour, neighbours);
      }
    }
  }

  return neighbour;
}

Node *newNeighbour(Node *root, Photon p, vector<Node*> neighbours)
{
  Node *nearest = NULL;

  if(neighbours.empty()){
    nearest = neighbour(root, p, nearest);
  }else{
    nearest = newNeighbourRec(root, p, 0, 0, nearest, neighbours);
  }

  return nearest;
}

vector<Node*> getNeighboursWithinRadius(Node *root, Photon p, float radius)
{
  vector<Node*> neighbours;
  int lastIndex = 0;
  float point[3] = {p.x, p.y, p.z};

  neighbours.push_back(newNeighbour(root, p, neighbours));
  while(true)
  {
    lastIndex = neighbours.size() - 1;
    if(neighbours[lastIndex] == NULL)
    {
      break;
    }
    if(neighbourDistance(neighbours[lastIndex]->point, point) > radius)
    {
      break;
    }
    vector<Node*> oldNeighbours (neighbours);
    neighbours.push_back(newNeighbour(root, p, neighbours));
    if(root == problem_root){
      cout << lastIndex << endl;
    }
  }
  vector<Node*> returnNeighbours (neighbours.begin(), neighbours.end() - 1);

  return returnNeighbours;
}

bool inNeighbours(Photon p, vector<Node*> neighbours)
{
  for(uint i=0; i < neighbours.size(); i++)
  {
    if(arePointsSame(p, neighbours[i]->p)) return true;
  }

  return false;
}

float neighbourDistance(float a[3], float b[3])
{
  return sqrt(pow((a[0] - b[0]), 2) +
              pow((a[1] - b[1]), 2) +
              pow((a[2] - b[2]), 2));
}

float coordDistance(float a, float b)
{
  return sqrt(pow((a - b), 2));
}

/*Place updates of parameters here*/
// ------------------ Camera controls ------------------
bool Update()
{
  /* Update variables*/
  SDL_Event e;
  while(SDL_PollEvent(&e))
  {
    if(e.type == SDL_QUIT)
    {
      return false;
    }
    else
    {
      if (e.type == SDL_KEYDOWN)
      {
        int key_code = e.key.keysym.sym;
        switch(key_code)
        {
          case SDLK_UP:
            /* move camera forward */
            cameraPos.z += MoveDistance;
            break;
          case SDLK_DOWN:
            /* move camera backwards */
            cameraPos.z -= MoveDistance;
            break;
          case SDLK_LEFT:
            /* move camera left */
            yaw -= rotationStep;
            R[0][0] = cos(yaw);
            R[2][0] = sin(yaw);
            R[0][2] = -sin(yaw);
            R[2][2] = cos(yaw);
            break;
          case SDLK_RIGHT:
            /* move camera right */
            yaw += rotationStep;
            R[0][0] = cos(yaw);
            R[2][0] = sin(yaw);
            R[0][2] = -sin(yaw);
            R[2][2] = cos(yaw);
            break;
          case SDLK_w:
            /* move light forward */
            if(moveSphere){
              spheres[0].center.z += lightMoveDistance;
              cout << "Sphere Pos: "; PrintVec4(spheres[0].center);
            }
            else{
              light1Pos.z += lightMoveDistance;
              cout << "Light Pos: "; PrintVec4(cameraPos);
            }
            break;
          case SDLK_s:
            /* move light forward */
            if(moveSphere){
              spheres[0].center.z -= lightMoveDistance;
              cout << "Sphere Pos: "; PrintVec4(spheres[0].center);
            }
            else{
              light1Pos.z -= lightMoveDistance;
              cout << "Light Pos: "; PrintVec4(cameraPos);
            }
            break;
          case SDLK_a:
            /* move light forward */
            if(moveSphere){
              spheres[0].center.x -= lightMoveDistance;
              cout << "Sphere Pos: "; PrintVec4(spheres[0].center);
            }
            else{
              light1Pos.x -= lightMoveDistance;
              cout << "Light Pos: "; PrintVec4(cameraPos);
            }
            break;
          case SDLK_d:
            /* move light forward */
            if(moveSphere){
              spheres[0].center.x += lightMoveDistance;
              cout << "Sphere Pos: "; PrintVec4(spheres[0].center);
            }
            else{
              light1Pos.x += lightMoveDistance;
              cout << "Light Pos: "; PrintVec4(cameraPos);
            }
            break;
          case SDLK_q:
            /* move light forward */
            if(moveSphere){
              spheres[0].center.y -= lightMoveDistance;
              cout << "Sphere Pos: "; PrintVec4(spheres[0].center);
            }
            else{
              light1Pos.y -= lightMoveDistance;
              cout << "Light Pos: "; PrintVec4(cameraPos);
            }
            break;
          case SDLK_e:
            /* move light forward */
            if(moveSphere){
              spheres[0].center.y += lightMoveDistance;
              cout << "Sphere Pos: "; PrintVec4(spheres[0].center);
            }
            else{
              light1Pos.y += lightMoveDistance;
              cout << "Light Pos: "; PrintVec4(cameraPos);
            }
            break;
          case SDLK_ESCAPE:
            /* Leave */
            cameraPos.x += MoveDistance;
            return false;
        }
      }
    }
  }
  return true;
}
