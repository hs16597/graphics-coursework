#ifndef TEST_MODEL_CORNEL_BOX_H
#define TEST_MODEL_CORNEL_BOX_H

// Defines a simple test model: The Cornel Box

// TODO Make colours not have zeroes

#include <glm/glm.hpp>
#include <vector>

// Used to describe a triangular surface:
class Triangle
{
public:
	glm::vec4 v0;
	glm::vec4 v1;
	glm::vec4 v2;
	glm::vec4 normal;
	glm::vec3 color;
	bool glass;
	float spec; //specular reflection
	float diff; //diffuse reflection
	float ri;   // refractive Index

	Triangle( glm::vec4 v0, glm::vec4 v1, glm::vec4 v2, glm::vec3 color, bool glass, float diff, float spec, float ri )
		: v0(v0), v1(v1), v2(v2), color(color), glass(glass), spec(spec), diff(diff), ri(ri)
	{
		ComputeNormal();
	}

	void ComputeNormal()
	{
	  glm::vec3 e1 = glm::vec3(v1.x-v0.x,v1.y-v0.y,v1.z-v0.z);
	  glm::vec3 e2 = glm::vec3(v2.x-v0.x,v2.y-v0.y,v2.z-v0.z);
	  glm::vec3 normal3 = glm::normalize( glm::cross( e2, e1 ) );
	  normal.x = normal3.x;
	  normal.y = normal3.y;
	  normal.z = normal3.z;
	  normal.w = 1.0;
	}
};

class Sphere
{
public:
	glm::vec4 center;
	float radius;
	glm::vec3 color;
	bool glass;
	float spec;
	float diff;
	float ri;

	Sphere(glm::vec4 center, float radius, glm::vec3 color, bool glass, float spec, float diff, float ri)
	: center(center), radius(radius), color(color), glass(glass), spec(spec), diff(diff), ri(ri)
	{}
};

void LoadTestSpheres( std::vector<Sphere>& spheres )
{
	using glm::vec4;
	using glm::vec3;

	vec3 red(    0.75f, 0.15f, 0.15f );
	vec3 white(  0.75f, 0.75f, 0.75f );
	spheres.clear();
	spheres.reserve(1);

	//Center, radius, color, glass, spec, diff, ri
	spheres.push_back( Sphere( glm::vec4(0, 0, -0.2, 0), 0.2, white, false, 1, 0, 1.4f ) );
}

// Loads the Cornell Box. It is scaled to fill the volume:
// -1 <= x <= +1
// -1 <= y <= +1
// -1 <= z <= +1
void LoadTestModel( std::vector<Triangle>& triangles )
{
	using glm::vec3;
	using glm::vec4;

	// Defines colors:
	vec3 red(    0.75f, 0.15f, 0.15f );
	vec3 yellow( 0.75f, 0.75f, 0.15f );
	vec3 green(  0.15f, 0.75f, 0.15f );
	vec3 cyan(   0.15f, 0.75f, 0.75f );
	vec3 blue(   0.15f, 0.15f, 0.75f );
	vec3 purple( 0.75f, 0.15f, 0.75f );
	vec3 white(  0.75f, 0.75f, 0.75f );

	triangles.clear();
	triangles.reserve( 5*2*3 );

	// ---------------------------------------------------------------------------
	// Room

	float L = 555;			// Length of Cornell Box side.

	vec4 A(L,0,0,1);
	vec4 B(0,0,0,1);
	vec4 C(L,0,L,1);
	vec4 D(0,0,L,1);

	vec4 E(L,L,0,1);
	vec4 F(0,L,0,1);
	vec4 G(L,L,L,1);
	vec4 H(0,L,L,1);

	float wallDiff = 0.5;
	float wallSpec = 0;
	// Floor: (V0, V1, V2, color, glass, diff, spec, refractive Index)
	//triangles.push_back( Triangle( C, B, A, green ) );
	triangles.push_back( Triangle( C, B, A, white, false, wallDiff, wallSpec, 1) );
	//triangles.push_back( Triangle( C, D, B, green ) );
	triangles.push_back( Triangle( C, D, B, white, false, wallDiff, wallSpec, 1 ) );

	// Left wall
	//triangles.push_back( Triangle( A, E, C, purple ) );
	triangles.push_back( Triangle (A, E, C, red, false,  wallDiff, wallSpec, 1) );
	//triangles.push_back( Triangle( C, E, G, purple ) );
	triangles.push_back( Triangle( C, E, G, red, false,  wallDiff, wallSpec, 1) );

	// Right wall
	// triangles.push_back( Triangle( F, B, D, yellow ) );
	triangles.push_back( Triangle( F, B, D, blue, false,  wallDiff, wallSpec, 1 ) );
	// triangles.push_back( Triangle( H, F, D, yellow ) );
	triangles.push_back( Triangle( H, F, D, blue, false,  wallDiff, wallSpec, 1 ) );

	// Ceiling
	// triangles.push_back( Triangle( E, F, G, cyan ) );
	triangles.push_back( Triangle( E, F, G, white, false,  wallDiff, wallSpec, 1) );
	// triangles.push_back( Triangle( F, H, G, cyan ) );
	triangles.push_back( Triangle( F, H, G, white, false,  wallDiff, wallSpec, 1) );

	// Back wall
	// triangles.push_back( Triangle( G, D, C, white ) );
	triangles.push_back( Triangle( G, D, C, white, false,  wallDiff, wallSpec, 1) );
	// triangles.push_back( Triangle( G, H, D, white ) );
	triangles.push_back( Triangle( G, H, D, white, false,  wallDiff, wallSpec, 1 ) );

	// ---------------------------------------------------------------------------
	// Short block

	A = vec4(290,0,114,1);
	B = vec4(130,0, 65,1);
	C = vec4(240,0,272,1);
	D = vec4( 82,0,225,1);

	E = vec4(290,165,114,1);
	F = vec4(130,165, 65,1);
	G = vec4(240,165,272,1);
	H = vec4( 82,165,225,1);

	float sbDiff = 0;
	float sbSpec = 0;
	vec3 sbCol = red;
	bool sbGlass = false;
	// Front
	triangles.push_back( Triangle(E,B,A, sbCol, sbGlass, sbDiff, sbSpec, 1) );
	triangles.push_back( Triangle(E,F,B, sbCol, sbGlass, sbDiff, sbSpec, 1) );

	// Front
	triangles.push_back( Triangle(F,D,B, sbCol, sbGlass, sbDiff, sbSpec, 1) );
	triangles.push_back( Triangle(F,H,D, sbCol, sbGlass, sbDiff, sbSpec, 1) );

	// BACK
	triangles.push_back( Triangle(H,C,D, sbCol, sbGlass, sbDiff, sbSpec, 1) );
	triangles.push_back( Triangle(H,G,C, sbCol, sbGlass, sbDiff, sbSpec, 1) );

	// LEFT
	triangles.push_back( Triangle(G,E,C, sbCol, sbGlass, sbDiff, sbSpec, 1) );
	triangles.push_back( Triangle(E,A,C, sbCol, sbGlass, sbDiff, sbSpec, 1) );

	// TOP
	triangles.push_back( Triangle(G,F,E, sbCol, sbGlass, sbDiff, sbSpec, 1) );
	triangles.push_back( Triangle(G,H,F, sbCol, sbGlass, sbDiff, sbSpec, 1) );

	// ---------------------------------------------------------------------------
	// Tall block
	A = vec4(423,0,247,1);
	B = vec4(265,0,296,1);
	C = vec4(472,0,406,1);
	D = vec4(314,0,456,1);

	E = vec4(423,330,247,1);
	F = vec4(265,330,296,1);
	G = vec4(472,330,406,1);
	H = vec4(314,330,456,1);

	float tbDiff = 0;
	float tbSpec = 0;
	vec3 tbCol = blue;
	bool tbGlass = false;

	// Front
	triangles.push_back( Triangle(E,B,A, tbCol, tbGlass, tbDiff, tbSpec, 1) );
	triangles.push_back( Triangle(E,F,B, tbCol, tbGlass, tbDiff, tbSpec, 1) );

	// Front
	triangles.push_back( Triangle(F,D,B, tbCol, tbGlass, tbDiff, tbSpec, 1) );
	triangles.push_back( Triangle(F,H,D, tbCol, tbGlass, tbDiff, tbSpec, 1) );

	// BACK
	triangles.push_back( Triangle(H,C,D, tbCol, tbGlass, tbDiff, tbSpec, 1) );
	triangles.push_back( Triangle(H,G,C, tbCol, tbGlass, tbDiff, tbSpec, 1) );

	// LEFT
	triangles.push_back( Triangle(G,E,C, tbCol, tbGlass, tbDiff, tbSpec, 1) );
	triangles.push_back( Triangle(E,A,C, tbCol, tbGlass, tbDiff, tbSpec, 1) );

	// TOP
	triangles.push_back( Triangle(G,F,E, tbCol, tbGlass, tbDiff, tbSpec, 1) );
	triangles.push_back( Triangle(G,H,F, tbCol, tbGlass, tbDiff, tbSpec, 1) );
	// ----------------------------------------------
	// Scale to the volume [-1,1]^3

	for( size_t i=0; i<triangles.size(); ++i )
	{
		triangles[i].v0 *= 2/L;
		triangles[i].v1 *= 2/L;
		triangles[i].v2 *= 2/L;

		triangles[i].v0 -= vec4(1,1,1,1);
		triangles[i].v1 -= vec4(1,1,1,1);
		triangles[i].v2 -= vec4(1,1,1,1);

		triangles[i].v0.x *= -1;
		triangles[i].v1.x *= -1;
		triangles[i].v2.x *= -1;

		triangles[i].v0.y *= -1;
		triangles[i].v1.y *= -1;
		triangles[i].v2.y *= -1;

		triangles[i].v0.w = 1.0;
		triangles[i].v1.w = 1.0;
		triangles[i].v2.w = 1.0;

		triangles[i].ComputeNormal();
	}
}

#endif
