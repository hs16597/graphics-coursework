The Rasteriser:
Screen based clipping is just clipping a step further down the pipeline to
frustum clipping. It allows for clipping in the depth axis as well as x and y. I
haven't managed to implement it however I adapted our code to do the proper
projections into world homogeneous, clipping space then into screen space so the
points are correct.

What needs to be done
either:
a. Move the clipping to be frustum based.
+ It shouldn't be too hard to adapt the line clipping code to 3D (I think??)
+ Would seem more impressive than screen clipping (although we have technically
  done both parts needed for frustum clipping, the projection & the clipping)
- Would take time to do (could be better spent on the raytracer?)
- Screen clipping almost works


a, Alt: Add frustrum based culling.
+ Don't have to fuss about 3D clipping
+ shows we know about the projection
- Basically doing clipping without the interpolation?

b. Fix the screen based clipping, ALMOST DONE
+ It basically works. Needs corners to be added. NEEDS TO BE DONE

+ Could argue we know how to do frustrum clipping to Carl and that I just f*cked
  up and did screen clipping.
- Need to make corner test specific to screen space
- Strange errors when you look too far left or right. Inverted image if you look
  behind

I would like to be able to say we've done it properly in frustum space however I
don't know how much longer that would take given the amount of time we have left
especially how things tend to get much more complex in the 3D space. I'd say
maybe give it a go and see if its feasable if not try and fix the screen space
clipping.

================================================================================
The Raytracer:
After a fair amount of searching I found that a photon mapper has 4 types of
light each calculated using different methods

Direct Diffuse: standard raytracer with shadow rays
Specular: Path tracer
Indirect Diffuse: Photon mapping

I think trying to implement a path tracer now wouldn't be great so instead we
should just ignore specular light and focus on diffuse and indirect diffuse. If
we get time we can add it later as its just part of a sum we can ommit.

What needs to be done:

- Adding the wavelength to photons for frensels equation.
  At the moment the color of the photon is either white if it is immiediately
  absorbed or the color of whatever it last bounced off. This is neither
  correct or what we want, its a placeholder.

  What we want to do is to have it reflect off surfaces dependent on its
  wavelength. Especially glass reflecting/ refracting. Does reflection and
  refraction both occur and if so can we use russian roulette to simulate this

  I don't know how this works and the physics gets far too wordy for me to
  figure out properly. I would like to have a function based on the
  wavelength and the color of the surface which returns either what angle it
  reflects, how likely it is to reflect or the change of wavelength.

  If that isn't possible then we can create a scene with a perfectly diffuse
  white surface with a prism infront to get the wavelength scattering so we
  don't have to deal with the colours.

I think getting a kind of crappy looking render with the wavelength implemented
photon map would be best either if it is of the scene or if we have to arrange
the scene i.e. one white perfectly diffuse surface and a glass prism

--------------------------------------------------------------------------------------------------
TODO MK2:
Rasteriser
- Add corners. know x, y values, need to interpolate 3D pos and zinv value (Force clip to corner?)

- Fix off-screen check (could do in frustrum clipping)


Raytracer

- Add sphere to scene

- Add importance map, shoot rays from light: if it hits caustic add true if not false
  - Only shoot rays that go through caustic map
  - Add to second photon map: caustic map

- Make reflection/ refraction (russian roulette?)
