#include <iostream>
#include <glm/glm.hpp>
#include <SDL.h>
#include "SDLauxiliary.h"
#include "TestModel.h"
#include <stdint.h>

using namespace std;
using glm::vec3;
using glm::mat3;

#define SCREEN_WIDTH 720
#define SCREEN_HEIGHT 720
#define FULLSCREEN_MODE false


/* ----------------------------------------------------------------------------*/
/* GLOBAL VARIABLES                                                            */
int t;
vector<vec3> stars(1000);

/* ----------------------------------------------------------------------------*/
/* FUNCTIONS                                                                   */

void Update();
void Draw(screen* screen);

int main( int argc, char* argv[] )
{
  for( uint i=0; i<stars.size(); i++)
  {
    float r1 = float(rand()) / float(RAND_MAX);
    float r2 = float(rand()) / float(RAND_MAX);
    float r3 = float(rand()) / float(RAND_MAX);

    stars.at(i).x = 2*r1-1;
    stars.at(i).y = 2*r2-1;
    stars.at(i).z = r3;
  }
  screen *screen = InitializeSDL( SCREEN_WIDTH, SCREEN_HEIGHT, FULLSCREEN_MODE );
  t = SDL_GetTicks();	/*Set start value for timer.*/

  while( NoQuitMessageSDL() )
    {
      Draw(screen);
      Update();
      SDL_Renderframe(screen);
    }

  SDL_SaveImage( screen, "screenshot.bmp" );

  KillSDL(screen);
  return 0;
}

/*Place your drawing here*/
void Draw(screen* screen)
{
  /* Clear buffer */
  memset(screen->buffer, 0, screen->height*screen->width*sizeof(uint32_t));
  for (size_t s=0; s<stars.size(); ++s)
  {
    float f = screen->height/2.0f;
    float u = f * (stars.at(s).x / stars.at(s).z) + screen->width/2.0f;
    float v = f * (stars.at(s).y / stars.at(s).z) + screen->height/2.0f;
    vec3 colour = vec3(1,1,1) / (stars.at(s).z * stars.at(s).z);
    PutPixelSDL(screen, u, v, colour);
  }
}

/*Place updates of parameters here*/
void Update()
{
  /* Compute frame time */
  int t2 = SDL_GetTicks();
  float dt = float(t2-t);
  t = t2;
  /* Update variables*/
  float v = 0.001;
  for( uint s=0; s<stars.size(); ++s)
  {
    // Update stars
    float oldZ = stars.at(s).z;
    stars.at(s).z = oldZ - v * dt;
    if (stars.at(s).z <= 0){
      stars.at(s).z += 1;
    }
    if( stars.at(s).z > 1){
      stars.at(s).z -= 1;
    }
  }
}
